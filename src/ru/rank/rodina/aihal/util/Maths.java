package ru.rank.rodina.aihal.util;

/**
 * Математические функции
 */
public class Maths {
    
    /**
     * Округление до определённого знака после запятой
     * @param value округляемеое число
     * @param length число знаков после запятой
     * @return оруглённое дробное число
     */
    public static double round(double value, double length)
    {
        double newValue=Math.pow(10,length); 
        return Math.round(value*newValue)/newValue; 
    }
}
