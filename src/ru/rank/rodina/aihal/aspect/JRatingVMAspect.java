package ru.rank.rodina.aihal.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import ru.rank.rodina.aihal.vm.JRatingVM;

/**
 *
 * @author Romansu
 */
@Aspect
public class JRatingVMAspect {
   
    @AfterReturning("@annotation(ru.rank.rodina.aihal.annotation.Setter)")    
    public void changeNewProperty(JoinPoint joinPoint){
        JRatingVM jr = (JRatingVM)joinPoint.getTarget();
        jr.setNewItem(false);		    
    }
}
