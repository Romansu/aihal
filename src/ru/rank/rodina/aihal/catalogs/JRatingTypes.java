package ru.rank.rodina.aihal.catalogs;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Имена показателей рейтинга RMR 
 */
public enum JRatingTypes {    
    A1 {
        @Override
        public Map<String, Integer> getAllValues() {
            Map<String, Integer> allValues = new LinkedHashMap();
            allValues.put(">250 МПа", 15);
            allValues.put("100-200 МПа", 12);
            allValues.put("50-100 МПа", 7);
            allValues.put("25-50 МПа", 4);
            allValues.put("5-25 МПа", 2);
            allValues.put("1-5 МПа", 1);
            allValues.put("<1 МПа", 0);            
            return allValues;
        }

        @Override
        public String getName() {
            return "Прочность породы на одноосное сжатие";
        }
    }, 
    A2 {
        @Override
        public Map<String, Integer> getAllValues() {
            Map<String, Integer> allValues = new LinkedHashMap();
            allValues.put("90-100%", 20);
            allValues.put("75-90%", 17);
            allValues.put("50-75%", 13);
            allValues.put("25-50%", 8);
            allValues.put("<25", 3);
            return allValues;
        }

        @Override
        public String getName() {
            return "Качество массива по выходу керна RQD";
        }
    }, 
    A3 {
        @Override
        public Map<String, Integer> getAllValues() {
            Map<String, Integer> allValues = new LinkedHashMap();
            allValues.put(">2м", 20);
            allValues.put("0,6-2м", 15);
            allValues.put("200-600 мм", 10);
            allValues.put("60-200 мм", 8);
            allValues.put("<60 мм", 5);
            return allValues;
        }

        @Override
        public String getName() {
            return "Расстояние между трещинами";
        }
    }, 
    A41 {
        @Override
        public Map<String, Integer> getAllValues() {
            Map<String, Integer> allValues = new LinkedHashMap();
            allValues.put("Очень шероховатые", 6);
            allValues.put("Слегка шероховатые", 5);
            allValues.put("Средне шероховатые", 3);
            allValues.put("Гладкие поверхности", 1);
            allValues.put("Следы скольжения", 0);
            return allValues;
        }

        @Override
        public String getName() {
            return "Шероховатость трещин";
        }
    }, 
    A42 {
        @Override
        public Map<String, Integer> getAllValues() {
            Map<String, Integer> allValues = new LinkedHashMap();
            allValues.put("<1м", 6);
            allValues.put("1-3м", 4);
            allValues.put("3-10м", 2);
            allValues.put("10-20м", 1);
            allValues.put(">20м", 0);
            return allValues;
        }

        @Override
        public String getName() {
            return "Длина трещин";
        }
    }, 
    A43 {
        @Override
        public Map<String, Integer> getAllValues() {
            Map<String, Integer> allValues = new LinkedHashMap();
            allValues.put("Нет", 6);
            allValues.put("<0,1 мм", 5);
            allValues.put("0,1-1 мм", 4);
            allValues.put("1-5 мм", 1);
            allValues.put(">5мм", 0);
            return allValues;
        }

        @Override
        public String getName() {
            return "Раскрытие трещин";
        }
    }, 
    A44 {
        @Override
        public Map<String, Integer> getAllValues() {
            Map<String, Integer> allValues = new LinkedHashMap();
            allValues.put("Нет", 6);
            allValues.put("Твёрдый заполнитель <5 мм", 5);
            allValues.put("Твёрдый заполнитель >5 мм", 4);
            allValues.put("Мягкий заполнитель <5 мм", 1);
            allValues.put("Мягкий заполнитель >5мм", 0);
            return allValues;
        }

        @Override
        public String getName() {
            return "Заполнитель трещин";
        }
    }, 
    A45 {
        @Override
        public Map<String, Integer> getAllValues() {
            Map<String, Integer> allValues = new LinkedHashMap();
            allValues.put("Нет", 6);
            allValues.put("Слегка выветрелые", 5);
            allValues.put("Средне выветрелые", 3);
            allValues.put("Сильно выветрелые", 1);
            allValues.put("Раздробленные", 0);
            return allValues;
        }

        @Override
        public String getName() {
            return "Выветрелость стенок трещин";
        }
    }, 
    A5 {
        @Override
        public Map<String, Integer> getAllValues() {
            Map<String, Integer> allValues = new LinkedHashMap();
            allValues.put("Полностью сухая", 15);
            allValues.put("Влажная", 10);
            allValues.put("Мокрая", 7);
            allValues.put("Капёж", 4);
            allValues.put("Водоприток", 0);
            return allValues;
        }

        @Override
        public String getName() {
            return "Обводненность выработки";
        }
    }, 
    B {
        @Override
        public Map<String, Integer> getAllValues() {
            Map<String, Integer> allValues = new LinkedHashMap();
            allValues.put("Очень благоприятная ", 0);
            allValues.put("Благоприятная", -2);
            allValues.put("Средняя", -5);
            allValues.put("Неблагоприятная", -10);
            allValues.put("Очень неблагоприятная", -12);
            return allValues;
        }

        @Override
        public String getName() {
            return "Ориентация трещин";
        }
    };
    
    public abstract Map<String, Integer>getAllValues();
    public abstract String getName();
}
