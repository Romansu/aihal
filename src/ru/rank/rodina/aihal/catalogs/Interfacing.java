package ru.rank.rodina.aihal.catalogs;

/**
 * Виды сопряжений
 */
public enum Interfacing {
    NO("Нет сопряжения") {
        @Override
        public int getDBValue() {
            return 0;
        }
    }, INTERSECTION("Пересечение") {
        @Override
        public int getDBValue() {
            return 1;
        }
    }, CONTIGUITY("Примыкание") {
        @Override
        public int getDBValue() {
            return 1;
        }
    };
    private String name;
    private Interfacing(String desc) {
        name = desc;
    }
    public String getName() {
        return name;
    }
        
    @Override
    public String toString(){
        return name;
    }
    
    /**
     * Возвращает значение для записи в БД
     * @return значение 0/1
     */
    public abstract int getDBValue();    
}
