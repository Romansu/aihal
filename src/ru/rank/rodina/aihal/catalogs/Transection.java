package ru.rank.rodina.aihal.catalogs;

/**
 * Виды сечений
 */
public enum Transection {
    RECTANGLE("Прямоугольное сечение"), VAULT("Сводчатое сечение"), 
    ARCH("Арочное сечение");
    private String name;
    private Transection(String desc) {
        name = desc;
    }
    public String getName() {
        return name;
    }
    
    @Override
    public String toString(){
        return name;
    }
}
