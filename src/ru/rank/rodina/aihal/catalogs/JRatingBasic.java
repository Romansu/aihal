package ru.rank.rodina.aihal.catalogs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.aspectj.lang.annotation.Aspect;

/**
 * Класс со справочной информацией по рейтингу RMR.
 * Если будет изменяться - вынести в Property или БД
 */

public class JRatingBasic {
    private final String name;
    private final Map<String, Integer> values;
    
    public JRatingBasic(JRatingTypes type) {
        this.name = type.getName();
        this.values = type.getAllValues();
    }
    
    public Map<String, Integer> getValues() {
        return values;
    }
    
    public String getName() {
        return name;
    }
    
    public List<String> getValuesString() {
        List<String> list = new LinkedList(values.keySet());
        return list;
    }
    
    public List<Integer> getValuesInteger() {
        List<Integer> list = new LinkedList(values.values());
        return list;
    }
    
    public int getIndexOfValue(int searching) {
        int i = 0;
        for (Map.Entry<String, Integer> entry : values.entrySet()) {
            i++;
            Integer value = entry.getValue();
            if (value == searching) return i;    
        }
        return 0;
    }    
   
    
}
