package ru.rank.rodina.aihal.catalogs;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import ru.rank.rodina.aihal.entities.manytomany.CalcVariable;
import ru.rank.rodina.aihal.ui.controls.LabeledTextFieldH;
import ru.rank.rodina.aihal.vm.CalcVM;

/**
 * Типы переменной в расчёте
 */
public enum VariableTypes {
    CONDITION {
        @Override
        public Node create(CalcVariable variable, CalcVM calcVM) {
            Text label = new Text(variable.getVariable().getName());
            label.setWrappingWidth(500);
            return label;
        }
    }, INT_CH {
        @Override
        public Node create(CalcVariable variable, CalcVM calcVM) {   
            HBox hBox = new HBox();
            hBox.setSpacing(10);
            hBox.setAlignment(Pos.CENTER_LEFT);
            String baseText = variable.getVariable().getName() + ": " +
                            variable.getVariable().getNameShort();  
            String warningText = " Значение должно находиться между " +
                            variable.getVariable().getValueMin() + " и " +
                            variable.getVariable().getValueMax();
            LabeledTextFieldH ltf = new LabeledTextFieldH(baseText);  
            Text ending = new Text(variable.getVariable().getNameEnd());
            ending.setWrappingWidth(50);
            ltf.setPropertiesForCalcPane();
            ltf.setText(String.format("%.1f", variable.getValue()));            
            ltf.setListener((observable, oldValue, newValue) -> {
                if(!variable.isValueCorrect(Double.parseDouble(newValue))) ltf.setLabelText(baseText + warningText);
                else ltf.setLabelText(baseText); 
                if(newValue.isEmpty()) variable.setValue(0.0);
                else variable.setValue(Double.parseDouble(newValue));            
            }); 
            
            return hBox;
        }
        
    }, INT_UNCH {
        @Override
        public Node create(CalcVariable variable, CalcVM calcVM) {
            HBox hBox = new HBox();
            hBox.setSpacing(10);
            hBox.setAlignment(Pos.CENTER_LEFT);
            LabeledTextFieldH ltf = new LabeledTextFieldH(variable.getVariable().getName() +
                                    ": " + variable.getVariable().getNameShort());
            Text ending = new Text(variable.getVariable().getNameEnd());
            ending.setWrappingWidth(50);
            ltf.setPropertiesForCalcPane();
            ltf.setEditable(false);
            ltf.setText(String.format("%.1f",variable.getValue())); 
            hBox.getChildren().addAll(ltf, ending);
            return hBox;
        }
    }, BT_NEXT {
        @Override
        public Node create(CalcVariable variable, CalcVM calcVM) {            
            calcVM.saveData(); 
            Button nextButton = new Button("Продолжить");
            nextButton.setOnAction((event) -> {calcVM.rewriteNextPane(variable.getValue().intValue()); });            
            return nextButton;
        }
    }, BT_STOP {
        @Override
        public Node create(CalcVariable variable, CalcVM calcVM) {
            Text label = new Text();
            label.setFill(Color.RED);
            StringBuilder sb = new StringBuilder("Продолжение невозможно");
            if (variable.getVariable().getType() == CONDITION ) sb.append(", условие не выполнено.");
            else sb.append(", некорректное значение.");
            label.setText(sb.toString());
            return label;
        }
    }, BT_PRINT {
        @Override
        public Node create(CalcVariable variable, CalcVM calcVM) {
            calcVM.saveData();  
            return new Button("Печать");
        }
    };
    
    public abstract Node create(CalcVariable variable, CalcVM calcVM);
    
    
}


