package ru.rank.rodina.aihal.ui;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;
import ru.rank.rodina.aihal.catalogs.Interfacing;
import ru.rank.rodina.aihal.catalogs.Transection;
import ru.rank.rodina.aihal.vm.ObtVM;


public class ObtParamPaneController  {
    private Stage stage;
    private ObtVM obtVM;
    
    //<editor-fold defaultstate="collapsed" desc="FXML"> 
    @FXML
    ComboBox interfacing;
    
    @FXML
    ComboBox transection;
    
    @FXML
    CheckBox GKV;
    
    @FXML
    TextField B;
    
    @FXML
    TextField B1;
    
    @FXML
    TextField B2;
    
    @FXML
    TextField height;
    
    @FXML
    Label labelB1;
    
    @FXML
    Label labelB2;
    
    //</editor-fold> 
    
    
    public ObtParamPaneController() {
        stage = new Stage();
    }
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
    public void setObtVM(ObtVM obtVM) {
        this.obtVM =obtVM;
        interfacing.getSelectionModel().select(this.obtVM.getInterfacing());
        transection.getSelectionModel().select(this.obtVM.getTransection());
        setObtBindings();
        setListeners();
    }
    
    public void setStage(Stage stage) {
        this.stage = stage;
    }
    
    public void setModalityAndOwner(Stage parentStage) {
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(parentStage);        
    }
    //</editor-fold>
   
    //<editor-fold defaultstate="collapsed" desc="FXML">       
    @FXML
    private void exit() {
        stage.close();
    }
    
    @FXML
    public void initialize() {
        interfacing.setItems( FXCollections.observableArrayList(Interfacing.values()));
        transection.setItems(FXCollections.observableArrayList(Transection.values()));
        setBasicBindings();       
    }
    //</editor-fold>
       
    //<editor-fold defaultstate="collapsed" desc="Bindings and other">
    private void setListeners() {   
        interfacing.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> { this.obtVM.setInterfacing((Interfacing)newValue);});
        transection.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> { this.obtVM.setTransection((Transection)newValue);});
    }
    
    private void setBasicBindings() {
        ObservableValue<Boolean> bindingVisible = getInterfacingBinding(false);
        labelB1.visibleProperty().bind(bindingVisible);
        B1.visibleProperty().bind(bindingVisible);
        labelB2.visibleProperty().bind(bindingVisible);
        B2.visibleProperty().bind(bindingVisible);
        B.editableProperty().bind(getInterfacingBinding(true));
    }
    
    private void setObtBindings() {
        B.textProperty().bindBidirectional(this.obtVM.bProperty(), new NumberStringConverter());
        B1.textProperty().bindBidirectional(this.obtVM.b1Property(), new NumberStringConverter());
        B2.textProperty().bindBidirectional(this.obtVM.b2Property(), new NumberStringConverter());
        height.textProperty().bindBidirectional(this.obtVM.heightProperty(), new NumberStringConverter()); 
        GKV.selectedProperty().bindBidirectional(this.obtVM.gkvProperty());
        
    }
    
    private ObservableValue<Boolean> getInterfacingBinding(boolean reversed) {
        if (reversed)
            return Bindings.when(interfacing.valueProperty().isEqualTo(Interfacing.NO)).then(true).otherwise(false);
        else
            return Bindings.when(interfacing.valueProperty().isEqualTo(Interfacing.NO)).then(false).otherwise(true);
    }
    //</editor-fold>
   
}
