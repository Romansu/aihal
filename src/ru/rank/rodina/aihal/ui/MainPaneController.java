package ru.rank.rodina.aihal.ui;

import java.io.IOException;
import static java.lang.System.out;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import ru.rank.rodina.aihal.Aihal;
import ru.rank.rodina.aihal.vm.ObtVM;

/**
 * Контроллер основного окна программы
 */
public class MainPaneController {
    
    @FXML
    private BorderPane borderPane;
    
    private AnchorPane centerPane;   
    private AnchorPane leftPane;
    private LeftPaneController leftPaneController;
    private CenterPaneController centerPaneController;
    
    public MainPaneController() {       
        
    }
    
    public CenterPaneController getCenterPaneController() {
        return centerPaneController;
    } 
    
    @FXML
    private void initialize() {        
        createAndSetLeftPane();
        createAndSetCenterPane(); 
    }
          
    private void createAndSetLeftPane() {
        try {
            // Загружаем сведения об адресатах.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Aihal.class.getResource("ui/LeftPane.fxml"));
            leftPane = (AnchorPane) loader.load();            
            leftPaneController = loader.getController(); 
            borderPane.setLeft(leftPane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }    
      
    private void createAndSetCenterPane() {
        try {
            // Загружаем сведения об адресатах.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Aihal.class.getResource("ui/CenterPane.fxml"));
            centerPane = (AnchorPane) loader.load();
            centerPaneController = loader.getController();
            borderPane.setCenter(centerPane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void updateCenterPaneByVM(ObtVM obtVM) {
        createAndSetCenterPane();
        centerPaneController.updateCenterPane(obtVM);
    }
    
}
