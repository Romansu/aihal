package ru.rank.rodina.aihal.ui;

import java.util.Collections;
import java.util.List;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;
import ru.rank.rodina.aihal.Aihal;
import ru.rank.rodina.aihal.entities.Rock;
import ru.rank.rodina.aihal.ui.controls.EditingCellFactory;
import ru.rank.rodina.aihal.vm.ObtRockVM;

/**
 * 
 */
public class ObtRockPaneController {

    private Stage stage;
    private ObservableList<ObtRockVM> rockData = FXCollections.observableArrayList();
    private ObservableList<Rock> rocks = FXCollections.observableArrayList();
    
    //<editor-fold defaultstate="collapsed" desc="FXML">
    
    @FXML
    TableView<ObtRockVM> rockTable;
    
    @FXML
    TableColumn<ObtRockVM, Integer> columnLevel;
//    TableColumn<ObtRockVM, String> columnLevel;
    
    @FXML
    TableColumn<ObtRockVM, Rock> columnRock;
    
    @FXML
    TableColumn<ObtRockVM, Integer> columnR;
    
    @FXML
    TableColumn<ObtRockVM, Integer> columnHeight;
    
    //</editor-fold>
    
    public ObtRockPaneController(){
        this.stage = new Stage();      
    }
    
    //<editor-fold defaultstate="collapsed" desc="get/set">    
    public void setObtRockVN(List<ObtRockVM> obtRockVM) {
        this.rockData.addAll(obtRockVM);
        initData();
    }
    
     public void setStage(Stage stage) {
        this.stage = stage;
    }
    
    public void setModalityAndOwner(Stage parentStage) {
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(parentStage);        
    }
     //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="FXML">
    @FXML
    public void initialize() {        
        rocks.addAll( ObtRockVM.getAllRocks());
    }  
    
    @FXML
    public void exit() {
        stage.close();
    }   
    
    @FXML
    public void addRock() {
        int newIndex = rockTable.getSelectionModel().getSelectedIndex() + 2;
        CenterPaneController ctrl = Aihal.getMainPaneController().getCenterPaneController();        
        rockData.add(ctrl.getMainObtVM().addRock(newIndex ));
        Collections.sort(rockData);        
    }
    
    @FXML
    public void deleteRock() {
        ObtRockVM deleting = rockTable.getSelectionModel().getSelectedItem();
        CenterPaneController ctrl = Aihal.getMainPaneController().getCenterPaneController();  
        rockData.remove(deleting);
        Collections.sort(rockData); 
        ctrl.getMainObtVM().deleteRock(deleting);
    }
    //</editor-fold>
    
    private void initData(){
        this.rockTable.setItems(rockData);
        setCellValueFactories();
        setCellFactories();
        setHandlers();        
    }
        
    //<editor-fold defaultstate="collapsed" desc="Bindings and other">    
    
    private void setCellFactories(){        
        columnLevel.setCellFactory(new EditingCellFactory<ObtRockVM, Integer>());
        columnHeight.setCellFactory(new EditingCellFactory<ObtRockVM, Integer>());
        columnRock.setCellFactory(ComboBoxTableCell.forTableColumn(rocks));
    } 
    
    private void setCellValueFactories(){
        columnLevel.setCellValueFactory((param) -> new SimpleIntegerProperty(param.getValue().getLevel()).asObject());    
        columnHeight.setCellValueFactory((param) -> new SimpleIntegerProperty(param.getValue().getHeight()).asObject());
        columnR.setCellValueFactory((param) -> new SimpleIntegerProperty(param.getValue().getRock().getResist()).asObject());
        columnRock.setCellValueFactory((param) -> new SimpleObjectProperty(param.getValue().getRock()));    
    }
    
    private void setHandlers(){
        columnLevel.setOnEditCommit(
        new EventHandler<CellEditEvent<ObtRockVM, Integer>>() {
            @Override
            public void handle(CellEditEvent<ObtRockVM, Integer> t) { 
                ((ObtRockVM) t.getTableView().getItems().get(
                    t.getTablePosition().getRow())
                    ).setLevel(strangeStringToInteger(t.getNewValue()));
                }
            }
        );    
        columnHeight.setOnEditCommit(
        new EventHandler<CellEditEvent<ObtRockVM, Integer>>() {
            @Override
            public void handle(CellEditEvent<ObtRockVM, Integer> t) { 
                ((ObtRockVM) t.getTableView().getItems().get(
                    t.getTablePosition().getRow())
                    ).setHeight(strangeStringToInteger(t.getNewValue()));
                }
            }
        ); 
        
        columnRock.setOnEditCommit(
        new EventHandler<CellEditEvent<ObtRockVM, Rock>>() {
            @Override
            public void handle(CellEditEvent<ObtRockVM, Rock> t) {
                ObtRockVM selected = ((ObtRockVM) t.getTableView().getItems().
                          get(t.getTablePosition().getRow()));
                selected.setRock(t.getNewValue());
                rockTable.refresh();
                }
            }
        );
    }
       
    //</editor-fold>
    
    private Integer strangeStringToInteger(Object oldValue) {
        Object newValue = (Object)oldValue;
        return Integer.parseInt(newValue.toString());
    }
    
}
