package ru.rank.rodina.aihal.ui.controls;

import java.util.function.UnaryOperator;
import java.util.regex.Pattern;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;

/**
 * TextField со вводом только чисел
 */
public class DigitalTextField extends TextField {
    
    public DigitalTextField() {
        super();
        TextField textField = new TextField();
        Pattern pattern = Pattern.compile("\\d*|\\d+\\,\\d*");
        TextFormatter formatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> {
            return pattern.matcher(change.getControlNewText()).matches() ? change : null;
        });
        textField.setTextFormatter(formatter);
    }
    
}
