package ru.rank.rodina.aihal.ui.controls;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import ru.rank.rodina.aihal.vm.AbstractVM;

/**
 * Вспомогательный класс для создания фабрики редактируемых ячеек
 */
public class EditingCellFactory<T extends AbstractVM, K> implements Callback<TableColumn<T, K>, TableCell<T, K>> {

   @Override
    public TableCell call(TableColumn p) {
        return new EditingCell<K>();
    }
    
    //Архив кода из отдельных pane
   /*
        Callback<TableColumn<PPEPaneVM, String>, TableCell<PPEPaneVM, String>> editingCellFactory =
             new Callback<TableColumn<PPEPaneVM, String>, TableCell<PPEPaneVM, String>>() {
                 public TableCell call(TableColumn p) {
                    return new EditingCell();
                 }
             };*/
    
}
