
package ru.rank.rodina.aihal.ui.controls;

import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.util.StringConverter;

/**
 * Вспомогателый элемент вида поле ввода + подпись
 */
public class LabeledTextFieldH extends HBox{
    
    Text label;
    DigitalTextField textField;
    
    //<editor-fold defaultstate="collapsed" desc="Конструкторы">
    public LabeledTextFieldH() {
        label = new Text();
        textField = new DigitalTextField();
        setProperties();
    }
    
    public LabeledTextFieldH(String labelText) {
        this();
        label.setText(labelText);
    }
    //</editor-fold>
    
    private void setProperties() {        
        setSpacing(10);
        setAlignment(Pos.CENTER_LEFT);
        getChildren().addAll(label, textField);    
    }
    
     public void setPropertiesForCalcPane() {
        this.label.setWrappingWidth(500);
        this.textField.setPrefWidth(50);
    }
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
    public void setLabelText(String text) {
        label.setText(text);
    }
    
    public void setPromptText(String text) {
        textField.setPromptText(text);
    }
    
    public void setText(String text) {
        textField.setText(text);
    }
    
    public void setBidirectionalBinding(Property other) {
        textField.textProperty().bindBidirectional(other);
    }
    
    public void setBidirectionalBinding(Property other, StringConverter converter) {
        textField.textProperty().bindBidirectional(other, converter);
    }
    
    public void setListener(ChangeListener<String> listener) {
        textField.textProperty().addListener(listener);
    }
    
    public void setEditable(boolean value) {
        textField.setEditable(value);
        textField.setStyle("-fx-background-color: grey;"); 
    }
    //</editor-fold>
    
}
