package ru.rank.rodina.aihal.ui;

import java.io.IOException;
import static java.lang.System.out;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.rank.rodina.aihal.Aihal;
import ru.rank.rodina.aihal.vm.ObtVM;

/**
 * Контроллер окна данных выработки
 */
public class ObtPaneController {
    
    //<editor-fold defaultstate="collapsed" desc="FXML">
    @FXML
    TextField labelName;
    @FXML
    Label labelJRating;
    @FXML
    Button buttonJRating;
    @FXML
    Label labelParams;
    @FXML
    Button buttonParams;
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="FXML">
    @FXML
    private void initialize() { 
    }
    
    @FXML
    private void addCalc() {
        if (obtVM.isReadyForCalc()) obtVM.addCalc();
        else { 
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle(null);
            a.setHeaderText(null);
            a.setContentText("Сначала заполните RMR, слои и параметры");
            a.showAndWait();
            };
    }
    
    @FXML
    private void openJRatingPane() {
        try {
            Stage stage = new Stage();
            stage.setOnHiding( event -> {obtVM.checkChangesJRating();} );
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("JRatingPane.fxml"));
            AnchorPane JRatingPane = (AnchorPane) loader.load(); 
            JRatingPaneController ctrl = loader.getController(); 
            ctrl.setJRatingVM(obtVM.getJRatingVM());
            ctrl.setModalityAndOwner(Aihal.getMainPaneStage());
            ctrl.setStage(stage);
            stage.setScene(new Scene(JRatingPane));
            stage.show();                       
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    private void openParamsPane() {
        try {
            Stage stage = new Stage();
           // stage.setOnHiding( event -> {obtVM.checkChangesJRating();} );
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("ObtParamPane.fxml"));
            AnchorPane obtParamPane = (AnchorPane) loader.load(); 
            ObtParamPaneController ctrl = loader.getController(); 
            ctrl.setObtVM(obtVM);
            ctrl.setModalityAndOwner(Aihal.getMainPaneStage());
            ctrl.setStage(stage);
            stage.setScene(new Scene(obtParamPane));
            stage.show();                       
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    private void openRocksPane() {
        try {
            Stage stage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("ObtRockPane.fxml"));
            AnchorPane obtParamPane = (AnchorPane) loader.load(); 
            ObtRockPaneController ctrl = loader.getController(); 
            ctrl.setObtRockVN(obtVM.getObtRockVMs());
            ctrl.setModalityAndOwner(Aihal.getMainPaneStage());
            ctrl.setStage(stage);
            stage.setScene(new Scene(obtParamPane));
            stage.show();                       
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    //</editor-fold>
   
    
    private ObtVM obtVM;
    
    public void setObtVM( ObtVM obtVM) {
        this.obtVM = obtVM;
        
        labelName.textProperty().bindBidirectional(this.obtVM.nameProperty());
        labelJRating.textProperty().bindBidirectional(this.obtVM.jRatingNameProperty());
        labelParams.textProperty().bindBidirectional(this.obtVM.paramsNameProperty());
    }
    
    public ObtVM getObtVM() {
        return obtVM;
    }
   
    
}
