package ru.rank.rodina.aihal.ui;

import static java.lang.System.out;
import java.util.ArrayList;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.rank.rodina.aihal.catalogs.JRatingBasic;
import ru.rank.rodina.aihal.catalogs.JRatingTypes;
import ru.rank.rodina.aihal.vm.JRatingVM;

/**
 * 
 */
public class JRatingPaneController  {
    
    private JRatingVM jRatingVM;
    private Stage stage;
    private ArrayList<JRatingBasic>jRatingBasics; 
            
    //<editor-fold defaultstate="collapsed" desc="FXML">
    @FXML
    Label A1Name;
    @FXML
    Label A2Name;
    @FXML
    Label A3Name;
    @FXML
    Label A41Name;
    @FXML
    Label A42Name;
    @FXML
    Label A43Name;
    @FXML
    Label A44Name;
    @FXML
    Label A45Name;
    @FXML
    Label A5Name;
    @FXML
    Label BName;
    @FXML
    ComboBox A1Value;
    @FXML
    ComboBox A2Value;
    @FXML
    ComboBox A3Value;
    @FXML
    ComboBox A41Value;
    @FXML
    ComboBox A42Value;
    @FXML
    ComboBox A43Value;
    @FXML
    ComboBox A44Value;
    @FXML
    ComboBox A45Value;
    @FXML
    ComboBox A5Value;
    @FXML
    ComboBox BValue;
    @FXML
    Button Save;
    @FXML
    Button Exit;    
    //</editor-fold >

    public JRatingPaneController() { 
        jRatingBasics = new ArrayList();
        stage = new Stage();
    }    
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
    public void setJRatingVM(JRatingVM jRatingVM) {
        this.jRatingVM = jRatingVM;
        jRatingVM.setChangedItem(false);
        loadData();
    }
    
    public void setStage(Stage stage) {
        this.stage = stage;
    }
    //</editor-fold>
    
    @FXML
    public void initialize() {
        createJRatingBasics();
        createLabels();
        createComboBoxes();
    } 
    
    /**
     * Не знаю, как прикрутить связывание
     */    
    private void loadData() {
        if (!jRatingVM.isNew())  {
            A1Value.getSelectionModel().select(jRatingBasics.get(0).getIndexOfValue(jRatingVM.getA1())-1);
            A2Value.getSelectionModel().select(jRatingBasics.get(1).getIndexOfValue(jRatingVM.getA2())-1);
            A3Value.getSelectionModel().select(jRatingBasics.get(2).getIndexOfValue(jRatingVM.getA3())-1);
            A41Value.getSelectionModel().select(jRatingBasics.get(3).getIndexOfValue(jRatingVM.getA41())-1);
            A42Value.getSelectionModel().select(jRatingBasics.get(4).getIndexOfValue(jRatingVM.getA42())-1);
            A43Value.getSelectionModel().select(jRatingBasics.get(5).getIndexOfValue(jRatingVM.getA43())-1);
            A44Value.getSelectionModel().select(jRatingBasics.get(6).getIndexOfValue(jRatingVM.getA44())-1);
            A45Value.getSelectionModel().select(jRatingBasics.get(7).getIndexOfValue(jRatingVM.getA45())-1);
            A5Value.getSelectionModel().select(jRatingBasics.get(8).getIndexOfValue(jRatingVM.getA5())-1);
            BValue.getSelectionModel().select(jRatingBasics.get(9).getIndexOfValue(jRatingVM.getB())-1);
        } 
    }
    
    private void saveData() {
        Pair selectedPair = (Pair)A1Value.getSelectionModel().getSelectedItem();
        jRatingVM.setA1(selectedPair.valueI);        
        selectedPair = (Pair)A2Value.getSelectionModel().getSelectedItem();
        jRatingVM.setA2(selectedPair.valueI);  
        selectedPair = (Pair)A3Value.getSelectionModel().getSelectedItem();
        jRatingVM.setA3(selectedPair.valueI);  
        selectedPair = (Pair)A41Value.getSelectionModel().getSelectedItem();
        jRatingVM.setA41(selectedPair.valueI);  
        selectedPair = (Pair)A42Value.getSelectionModel().getSelectedItem();
        jRatingVM.setA42(selectedPair.valueI);  
        selectedPair = (Pair)A43Value.getSelectionModel().getSelectedItem();
        jRatingVM.setA43(selectedPair.valueI);  
        selectedPair = (Pair)A44Value.getSelectionModel().getSelectedItem();
        jRatingVM.setA44(selectedPair.valueI);  
        selectedPair = (Pair)A45Value.getSelectionModel().getSelectedItem();
        jRatingVM.setA45(selectedPair.valueI);  
        selectedPair = (Pair)A5Value.getSelectionModel().getSelectedItem();
        jRatingVM.setA5(selectedPair.valueI);  
        selectedPair = (Pair)BValue.getSelectionModel().getSelectedItem();
        jRatingVM.setB(selectedPair.valueI); 
        jRatingVM.setChangedItem(true);
        jRatingVM.setNewItem(false);
    }
    
    private void createLabels(){        
        A1Name.setText(jRatingBasics.get(0).getName());
        A2Name.setText(jRatingBasics.get(1).getName());
        A3Name.setText(jRatingBasics.get(2).getName());
        A41Name.setText(jRatingBasics.get(3).getName());
        A42Name.setText(jRatingBasics.get(4).getName());
        A43Name.setText(jRatingBasics.get(5).getName());
        A44Name.setText(jRatingBasics.get(6).getName());
        A45Name.setText(jRatingBasics.get(7).getName());
        A5Name.setText(jRatingBasics.get(8).getName());
        BName.setText(jRatingBasics.get(9).getName());
    }

    private void createComboBoxes() {
        A1Value.setItems(getObservableValues(jRatingBasics.get(0)));
        A2Value.setItems(getObservableValues(jRatingBasics.get(1)));
        A3Value.setItems(getObservableValues(jRatingBasics.get(2)));
        A41Value.setItems(getObservableValues(jRatingBasics.get(3)));
        A42Value.setItems(getObservableValues(jRatingBasics.get(4)));
        A43Value.setItems(getObservableValues(jRatingBasics.get(5)));
        A44Value.setItems(getObservableValues(jRatingBasics.get(6)));
        A45Value.setItems(getObservableValues(jRatingBasics.get(7)));
        A5Value.setItems(getObservableValues(jRatingBasics.get(8)));
        BValue.setItems(getObservableValues(jRatingBasics.get(9)));
    }
    
    private void createJRatingBasics() {
        JRatingTypes[] types = JRatingTypes.values();
        for (JRatingTypes type : types) {
            jRatingBasics.add(new JRatingBasic(type));
        }
    }
        
    private ObservableList<Pair> getObservableValues(JRatingBasic basic) {
         ObservableList<Pair> newList = FXCollections.observableArrayList();
         Map<String, Integer> values = basic.getValues();
         for (Map.Entry value : values.entrySet()) {
            newList.add(new Pair((String)value.getKey(), (int)value.getValue()));
         }         
         return newList;        
    }
    
    private boolean isCompleted() {
        return !(A1Value.getSelectionModel().isEmpty()||
            A2Value.getSelectionModel().isEmpty()||
            A3Value.getSelectionModel().isEmpty()||
            A41Value.getSelectionModel().isEmpty()||
            A42Value.getSelectionModel().isEmpty()||
            A43Value.getSelectionModel().isEmpty()||
            A44Value.getSelectionModel().isEmpty()||
            A45Value.getSelectionModel().isEmpty()||
            A5Value.getSelectionModel().isEmpty()||
            BValue.getSelectionModel().isEmpty());  
    }
    
    private class Pair {
        public String valueS;
        public int valueI; 
        public Pair(String s, int i) {
            valueS = s;
            valueI = i;
        }
        @Override
        public String toString() {
            return valueS;
        }
        @Override
        public boolean equals(Object other){
            if (other == null) return false;
            if (other == this) return true;
            if (!(other instanceof Pair))return false;
            Pair otherP = (Pair)other;
            if (this.valueI == otherP.valueI 
                && this.valueS == otherP.valueS) return true;
            return false;    
        }    
    }
   
    /**
     * Делает окно модальным и устанавливает владельца
     * @param parentStage владелец модального окна
     */
    public void setModalityAndOwner(Stage parentStage) {
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(parentStage);        
    }    
    
    /**
     * Нет смысла загружать данные по мере их изменения.
     */
    @FXML
    private void save() { 
        if (isCompleted()) {saveData();};
        stage.close();
    }
    
    @FXML
    private void exit() {
        stage.close();
    }

    
}
