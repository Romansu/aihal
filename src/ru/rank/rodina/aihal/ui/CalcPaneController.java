package ru.rank.rodina.aihal.ui;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;
import ru.rank.rodina.aihal.Aihal;
import ru.rank.rodina.aihal.entities.Method;
import ru.rank.rodina.aihal.vm.CalcVM;

/**
 * Контроллер панели расчётов
 */
public class CalcPaneController{
    
    private Tab tab;
    private CalcVM calcVM;
    
    //<editor-fold defaultstate="collapsed" desc="FXML">
    @FXML
    private ComboBox mainSupport;
    @FXML
    private ComboBox altSupport;
    @FXML
    private VBox vBox;    
    //</editor-fold>
   
    //<editor-fold defaultstate="collapsed" desc="FXML">
    @FXML
    public void initialize() {
    } 
    
    @FXML
    private void deleteCalc() {
        CenterPaneController ctrl = Aihal.getMainPaneController().getCenterPaneController();
        ctrl.getMainObtVM().deleteCalc(calcVM);
        ctrl.removeTab(tab);        
    }
    
    @FXML
    private void startCalculation(){
        if (mainSupport.getSelectionModel().getSelectedIndex() < 0 &&
            altSupport.getSelectionModel().getSelectedIndex() <0) {
            Alert a = new Alert(Alert.AlertType.WARNING);
            a.setTitle(null);
            a.setHeaderText(null);
            a.setContentText("Расчёт возможен только при заполненных методах");
            a.showAndWait();} 
        else {
            calcVM.deleteAndCreateValues(); 
        }
    }
    //</editor-fold> 
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
    
    public void setTab(Tab tab) {
        this.tab = tab;
    }
    
    public void setCalcVM(CalcVM calcVM) {
        this.calcVM = calcVM;
        this.calcVM.setCtrl(this);
        this.calcVM.createVariablesPane();
        loadMethods(); 
        loadData();
        setListeners();        
    }    
    //</editor-fold>
    
    public void addToVB(Node node) {          
        vBox.getChildren().add(node);
    }
    
    public void clearVB() {
        vBox.getChildren().clear();
    }
    
    public void crearVBChildren(int index) {
        ObservableList<Node> children = vBox.getChildren();
        if (children.size() >= index + 1) children.remove(index, children.size());      
    }
    
    private void loadMethods() {
        mainSupport.setItems(FXCollections.observableArrayList(calcVM.getAllMethods(Boolean.TRUE)));
        altSupport.setItems(FXCollections.observableArrayList(calcVM.getAllMethods(Boolean.FALSE)));
    }
    
    private void loadData() {
        mainSupport.getSelectionModel().select(calcVM.getMainMethod());
        altSupport.getSelectionModel().select(calcVM.getAdditionalMethod());
    }
    
    //<editor-fold defaultstate="collapsed" desc="Bindings and other">
    private void setListeners() {
        mainSupport.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> { calcVM.setMainMethod((Method)newValue); 
                calcVM.deleteValues();});
        altSupport.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {calcVM.setAdditionalMethod((Method)newValue); calcVM.deleteValues();});        
    }
    //</editor-fold>

}


