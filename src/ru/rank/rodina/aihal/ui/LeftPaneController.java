package ru.rank.rodina.aihal.ui;

import static java.lang.System.out;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import ru.rank.rodina.aihal.Aihal;
import ru.rank.rodina.aihal.vm.ObtVM;

/**
 * Контроллер левой панели основного окна, содержащего список доступных выработок, с базовыми действиями
 */
public class LeftPaneController {
    
    private ObservableList<ObtVM> obtData = FXCollections.observableArrayList();  
     
    //<editor-fold defaultstate="collapsed" desc="FXML">
    @FXML
    private TableView<ObtVM> obtTable;
    @FXML
    private TableColumn<ObtVM, Integer> columnId;
    @FXML
    private TableColumn<ObtVM, String> columnName;
    @FXML
    private MenuItem itemAdd;
    @FXML
    private MenuItem itemDelete;
    //</editor-fold>
         
    //<editor-fold defaultstate="collapsed" desc="FXML">
    @FXML
    private void initialize() {
        // Инициализация таблицы адресатов с двумя столбцами.
        columnId.setCellValueFactory( cellData -> new SimpleIntegerProperty(cellData.getValue().getId()).asObject());
        columnName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());        
        
        obtTable.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> updateCenterPane(obtTable.getSelectionModel().getSelectedItem()));
        
        initData();        
        obtTable.setItems(obtData);
    }
    
    @FXML
    private void addObt() {
        obtData.add(ObtVM.addObt());
    }
    
    @FXML
    private void deleteObt() {
        ObtVM deleting = obtTable.getSelectionModel().getSelectedItem();
        deleting.deleteObt();
        obtData.remove(deleting);
        //obtTable.getSelectionModel().
        
    }
    //</editor-fold>
        
    /**
     * Загружает из бд доступные выработки
     */
    private void initData() {        
        obtData = ObtVM.getAll();
    }
    
     /**
      * Передает выбранную VM контроллеру главного окна для обновление его центральной части
      * @param selectedVM выбранная в таблице VM 
      */
    private void updateCenterPane(ObtVM selectedVM) {
        MainPaneController mainPaneController = Aihal.getMainPaneController();
        mainPaneController.updateCenterPaneByVM(selectedVM);
    } 
    
    
    
}
