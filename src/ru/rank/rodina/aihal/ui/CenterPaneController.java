package ru.rank.rodina.aihal.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import ru.rank.rodina.aihal.vm.CalcVM;
import ru.rank.rodina.aihal.vm.ObtVM;

/**
 * Контроллер центральной части основного окна
 */
public class CenterPaneController {
    
    //<editor-fold defaultstate="collapsed" desc="FXML">
    @FXML
    private TabPane tabPane;    
    @FXML
    private Tab tabObt;
    //</editor-fold>
    
    private ObtPaneController obtPaneController;
    private List<CalcPaneController> calcPaneControllers;
    
    public CenterPaneController() { 
        this.calcPaneControllers = new ArrayList();
    }
    
    @FXML
    private void initialize() {        
    }
    
    public ObtVM getMainObtVM() {
        return obtPaneController.getObtVM();
    }
    
    public void updateCenterPane(ObtVM obtVM) {        
        createAndSetObtPane(obtVM);
        createAndSetCalcPane(obtVM);        
    }
    
    public void removeTab(Tab deletingTab){
        tabPane.getTabs().remove(deletingTab);        
    }
    /**
     * Создаёт окно Obt, привязывает к нему VM и сохраняет контроллер окна
     * @param obtVM VM для окна Obt
     */
    private void createAndSetObtPane(ObtVM obtVM) {
         try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("ObtPane.fxml"));
            AnchorPane obtPane = (AnchorPane) loader.load();            
            obtPaneController = loader.getController(); 
            obtPaneController.setObtVM(obtVM);
            tabObt.setContent(obtPane);
        } catch (IOException e) {
            e.printStackTrace();
        }        
        //tf.textProperty().bindBidirectional(this.obtVM.nameProperty()); 
    }
    
    private void createAndSetCalcPane(ObtVM obtVM) {
        if (obtVM.getCalcVMs() == null) obtVM.loadCalcVMs();
        else if(obtVM.getCalcVMs().isEmpty() ) obtVM.loadCalcVMs();
        try {
            
            List <CalcVM> calcVMs = obtVM.getCalcVMs();
            for (CalcVM element : calcVMs) {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("CalcPane.fxml")); 
                AnchorPane calcPane = (AnchorPane) loader.load();   
                CalcPaneController calcPaneController = loader.getController(); 
                calcPaneController.setCalcVM(element);
                Tab calcTab = new Tab("Расчёт №" + element.getId(), calcPane);
                calcPaneController.setTab(calcTab);
                tabPane.getTabs().add(calcTab); 
                
            }
        } catch (IOException e) {
            e.printStackTrace();
        }      
        //tf.textProperty().bindBidirectional(this.obtVM.nameProperty()); 
    }
    
    
    
}
