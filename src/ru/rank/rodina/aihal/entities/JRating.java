package ru.rank.rodina.aihal.entities;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Objects;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 * Сущность для учёта рейтинговых показателей массива горных пород RMR с базовой логикой расчётов
 */

@Entity
@Table(name = "JRATING")
public class JRating extends AbstractDB{
    
    //<editor-fold defaultstate="collapsed" desc="Columns">
    @Id
    @SequenceGenerator(name="seqJRating",sequenceName="JRATING_SEQ")        
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seqJRating")  
    private int id;
     
    @Column    
    private int A1;
    @Column
    private int A2;
    @Column
    private int A3;
    @Column
    private int A41;
    @Column
    private int A42;
    @Column
    private int A43;
    @Column
    private int A44;
    @Column
    private int A45;
    @Column
    private int A5;
    @Column
    private int B;
    
    //</editor-fold>
        
    @OneToOne (optional=false, cascade=CascadeType.ALL)
    @JoinColumn (name="obt_id")
    private Obt obt;
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
    public int getByName(String name){
        switch(name) {
            case "A1": 
                return A1;
            case "A2": 
                return A2;
            case "A3": 
                return A3;
            case "A41": 
                return A41;
            case "A42": 
                return A42;
            case "A43": 
                return A43;
            case "A44": 
                return A44;
            case "A45": 
                return A45;    
            case "A5": 
                return A5;
            case "B": 
                return B;    
	default: 
	    return 0;}
    }
    
    public void setByName(String name, int value){
        switch(name) {
            case "A1": 
                A1 = value;
                break;
            case "A2": 
                A2 = value;
                break;
            case "A3": 
                A3 = value;
                break;
            case "A41": 
                A41 = value;
                break;
            case "A42": 
                A42 = value;
                break;
            case "A43": 
                A43 = value;
                break;
            case "A44": 
                A44 = value;
                break;
            case "A45": 
                A45 = value;
                break;
            case "A5": 
                A5 = value;
                break;
            case "B": 
                B = value;
                break;
	default: 
	    break;}
    }        
    
    public void setObt(Obt obt) {
        this.obt = obt;
    }
    
    public int getSum() {
        return A1+ A2 + A3 + A5 + B +
               A41 + A42 + A43 + A44 + A45;
    }
    
    public int getRating(){
        int sum = getSum();
        if (sum<=100 && sum >= 81 ) return 1;
        else if (sum<= 80 && sum >= 61 ) return 2;
        else if (sum<= 60 && sum >= 41 ) return 3; 
        else if (sum<= 40 && sum >= 21 ) return 4;
        else return 5;
    }
    //</editor-fold>
     
    //<editor-fold defaultstate="collapsed" desc="Override">
    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }    
    //</editor-fold>

}
