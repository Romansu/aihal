package ru.rank.rodina.aihal.entities;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Сущность для работы с таблицей методов
 */
@Entity
@Table(name = "METHOD")
public class Method extends AbstractDB{
    
    //<editor-fold defaultstate="collapsed" desc="Columns">
    @Id
    @SequenceGenerator(name="seqMethod",sequenceName="METHOD_SEQ")        
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seqMethod")  
    private int id;
    
    @Column
    String name;
    
    @Column
    String scheme;
    //</editor-fold>
   
    //<editor-fold defaultstate="collapsed" desc="@OneTo">
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "METHOD_VARIABLE", joinColumns = @JoinColumn(name = "METHOD_ID"), inverseJoinColumns =  @JoinColumn(name = "VARIABLE_ID"))
    private List<Variable> variables;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Конструкторы">
    public Method() {
        this.variables = new LinkedList();
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
     public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    } 
    
    public String getScheme() {
        return this.scheme;
    }
    
    public void setScheme(String scheme) {
        this.scheme = scheme;
    }  
    
    public List <Variable> getVariables() {
        return variables;
    }
    //</editor-fold>
     
    //<editor-fold defaultstate="collapsed" desc="Override">
    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    } 
    
    @Override
    public String toString() {
        return name;
    }
    //</editor-fold>
}
