package ru.rank.rodina.aihal.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import ru.rank.rodina.aihal.catalogs.VariableTypes;

/**
 * Сущность для работы с таблицей переменных
 */
@Entity
@Table(name = "Variable")
public class Variable extends AbstractDB implements Comparable<Variable>{
    
    //<editor-fold defaultstate="collapsed" desc="Columns">
    @Id
    @SequenceGenerator(name="seqVariable",sequenceName="VARIABLE_SEQ")        
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seqVariable")  
    private int id;
    
    @Column
    private String name;
    
    @Column(name = "NAME_SHORT")
    private String nameShort;
    
    @Column(name = "NAME_END")
    private String nameEnd;
    
    @Column(name = "DEFAULT_VALUE")
    private Double valueDefault;
    
    @Column(name = "MIN_VALUE")
    private Double valueMin;
    
    @Column(name = "MAX_VALUE")
    private Double valueMax;
    
    @Column(name = "NPP")
    private Integer npp;
    
    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    private VariableTypes type;
    
    @Column(name = "METHOD")
    private String method;    
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    } 
    
    public String getNameShort() {
        if (nameShort == null) return "";
        else return nameShort;
    }
    public void setNameShort(String name) {
        this.nameShort = name;
    } 
    
    public String getNameEnd() {
        if (nameEnd == null) return "";
        return this.nameEnd;
    }
    public void setNameEnd(String name) {
        this.nameEnd = name;
    } 
    
    public Double getValueDefault() {
        return this.valueDefault;
    }
    public void setValueDefault(Double value) {
        this.valueDefault = value;
    } 
    
    public Double getValueMin() {
        return this.valueMin;
    }
    public void setValueMin(Double value) {
        this.valueMin = value;
    } 
    
    public Double getValueMax() {
        return this.valueMax;
    }
    public void setValueMax(Double value) {
        this.valueMax = value;
    } 
    
    public VariableTypes getType() {
        return this.type;
    }
    
    public void setType(VariableTypes type) {
        this.type = type;
    }
    
    public String getMethod() {
        return this.method;
    }    
    
    public Integer getNPP() {
        return this.npp;
    }
    //</editor-fold>
     
    //<editor-fold defaultstate="collapsed" desc="Override">
    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }    
    //</editor-fold> 

    @Override
    public int compareTo(Variable o) {        
        if(this.npp > o.getNPP()) return 1;
        else if(this.npp < o.getNPP()) return -1;
        else { if(this.id > o.getId()) return 1;
             else if(this.id < o.getId()) return -1;
             else return 0;
        }               
    }
}
