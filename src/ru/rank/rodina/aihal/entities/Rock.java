package ru.rank.rodina.aihal.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Сущность для работы с таблицей почв
 */
@Entity
@Table(name = "ROCK")
public class Rock extends AbstractDB{
    
    //<editor-fold defaultstate="collapsed" desc="Columns">
    @Id
    @SequenceGenerator(name="seqRock",sequenceName="ROCK_SEQ")        
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seqRock")   
    private int id; 
    
    @Column
    private String name;
    
    @Column
    private int resist;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    } 
    
    public int getResist() {
        return this.resist;
    }
    
    public void setResist(int resist) {
        this.resist = resist;
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Override">
    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }    
    
    @Override
    public String toString() {
        return name;
    }
    //</editor-fold>
    
}
