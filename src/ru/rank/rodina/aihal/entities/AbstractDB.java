package ru.rank.rodina.aihal.entities;

import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Абстрактный класс для работы с бд.
 * id всё равно переопределять, чтобы последовательности в бд отрабатывали
 */
//@MappedSuperclass
public abstract class AbstractDB {
  /*  @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    protected int id;*/
   /* 
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }*/
    public abstract int getId();
    public abstract void setId(int id);
}
