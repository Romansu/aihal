package ru.rank.rodina.aihal.entities;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ru.rank.rodina.aihal.entities.manytomany.CalcVariable;

/**
 * Сущность для работы с таблицей расчётов
 */
@Entity
@Table (name="CALCULATION")
public class Calc extends AbstractDB {
    
   //<editor-fold defaultstate="collapsed" desc="Columns">
    @Id
    @SequenceGenerator(name="seqCalc",sequenceName="CALC_SEQ", allocationSize = 1)        
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seqCalc")  
    private int id;
    
    @ManyToOne
    @JoinColumn(name = "obt_id", nullable = false)
    private Obt obt;
   
    
    @OneToOne (optional=true, cascade=CascadeType.REFRESH)
    @JoinColumn (name="main_method_id", nullable = true )
    @Fetch(FetchMode.SELECT)
    private Method methodMain;    
    
    @OneToOne (optional=true, cascade=CascadeType.REFRESH)
    @JoinColumn (name="ADDITIONAL_METOD_ID", nullable = true)  
    @Fetch(FetchMode.SELECT)            
    private Method methodAdditional;
    
    //TODO Победить множественные select (проблема N+1?)
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "calculation", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CalcVariable> variables;
    //</editor-fold>
    
   //<editor-fold defaultstate="collapsed" desc="get/set">
    public Obt getObt() {
        return this.obt;
    }
    
    public void setObt(Obt obt) {
        this.obt = obt;
    }
    
    public void setMethodMain(Method value) {
        methodMain = value;
    }
    
    public void setMethodAdditional(Method value) {
        methodAdditional = value;
    }
    
    public Method getMethodMain(){
        return methodMain;
    }
    
    public Method getMethodAdditional() {
        return methodAdditional;
    }
    
    public List<CalcVariable> getCalcVariables() {
        Collections.sort(variables);
        return variables;
    }
    
    public void setCalcVariables(List<CalcVariable> values) {
        this.variables = values;
    }
    
    public List<Variable> getVariables() {
        LinkedList<Variable> onlyVariables = new LinkedList();
        if (variables == null) return onlyVariables;
        for( CalcVariable value:variables) {
            onlyVariables.add(value.getVariable());
        }
        Collections.sort(onlyVariables);
        return onlyVariables;
    }
    
    public void setVariables(List<Variable> values) {
        if (variables == null) variables = new LinkedList();
        for(Variable value:values) {
            variables.add(new CalcVariable(this, value));
        }
    }
    
    public Map<String, CalcVariable> getVariablesWithMethods() {
        HashMap<String, CalcVariable> map = new HashMap();
        if (variables == null) return map;
        for(CalcVariable value:variables) {
            map.put(value.getVariable().getMethod(), value);
        }
        return map;
    }
    //</editor-fold>

   //<editor-fold defaultstate="collapsed" desc="Override">
    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }    
    //</editor-fold>
   
    
    public void clearVariables() {
        if (variables !=null) variables.clear();
    }
    
    public boolean isVariablesEmpty() {
        if (variables == null) return true;
        else return variables.isEmpty();
    }
    
}
