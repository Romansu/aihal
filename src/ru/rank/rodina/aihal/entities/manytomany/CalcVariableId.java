package ru.rank.rodina.aihal.entities.manytomany;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Составной ключ для таблицы CALCULATION_VARIABLE
 */
@Embeddable
public class CalcVariableId implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="Columns">
    @Column(name = "CALCULATION_ID")
    private Integer calculationId;
    
    @Column(name = "VARIABLE_ID")
    private Integer variableId;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Конструктор">
    private CalcVariableId() {
        
    }
    
    public CalcVariableId(Integer calculationId, Integer variableId) {
        this.calculationId = calculationId;
        this.variableId = variableId;
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Override">
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
 
        if (o == null || getClass() != o.getClass()) 
            return false;
 
        CalcVariableId that = (CalcVariableId) o;
        return Objects.equals(calculationId, that.calculationId) && 
               Objects.equals(variableId, that.variableId);
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(calculationId, variableId);
    }
    
    //</editor-fold>
    
}
