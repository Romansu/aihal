package ru.rank.rodina.aihal.entities.manytomany;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import ru.rank.rodina.aihal.entities.AbstractDB;
import ru.rank.rodina.aihal.entities.Obt;
import ru.rank.rodina.aihal.entities.Rock;

/**
 * Сущность для работы с таблицей связи выработка/сущность
 */
@Entity
@Table(name = "OBT_ROCK")
public class ObtRock extends AbstractDB{
    
    //<editor-fold defaultstate="collapsed" desc="Columns">
    @Id
    @SequenceGenerator(name="seqObtRock",sequenceName="OBT_ROCK_SEQ", allocationSize = 1)        
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seqObtRock")   
    private int id;     
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "OBT_ID")
    Obt obt;
    
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "ROCK_ID")
    private Rock rock;
    
    @Column(name = "lvl")
    int level;
    
    @Column
    int height;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Конструкторы">
    public ObtRock() {        
    }
    
    public ObtRock(Obt obt, Rock rock){
        this.obt = obt;
        this.rock = rock;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="get/set">
    public Obt getObt() {
        return obt;
    }
    
    public void setObt(Obt value) {
        this.obt = value;
    }
    
    public Rock getRock() {
        return rock;
    }
    
    public void setRock(Rock value) {
        this.rock = value;
    }
    
    public int getLevel() {
        return level;
    }
    
    public void setLevel(int value) {
        this.level = value;
    }
    
    public int getHeight() {
        return height;
    }
    
    public void setHeight(int value) {
        this.height = value;
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Override">
    @Override
    public int getId() {
        return 0;
    }

    @Override
    public void setId(int id) {        
    }
    //</editor-fold>

}
