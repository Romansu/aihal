package ru.rank.rodina.aihal.entities.manytomany;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import ru.rank.rodina.aihal.catalogs.VariableTypes;
import ru.rank.rodina.aihal.entities.Calc;
import ru.rank.rodina.aihal.entities.Variable;

/**
 * Сущность для работы с таблицей
 */
@Entity(name = "CalcVariable")
@Table(name = "CALCULATION_VARIABLE")
public class CalcVariable implements Serializable, Comparable<CalcVariable>{
    
    //<editor-fold defaultstate="collapsed" desc="Columns">
    @EmbeddedId
    private CalcVariableId id;
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @MapsId("calculationId")
    Calc calculation;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("variableId")
    Variable variable;
    
    @Column
    Double value;
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Конструкторы">
    private CalcVariable(){
        
    }
    
    public CalcVariable(Calc calculation, Variable variable) {
        this.calculation = calculation;
        this.variable = variable;
        this.id = new CalcVariableId(calculation.getId(), variable.getId());
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
    public Calc getCalc() {
        return this.calculation;
    }
    
    public Variable getVariable() {
        return this.variable;
    }

    public Double getValue() {
        return this.value;
    }
    
    public void setValue(Double value) {
        this.value = value;
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Override">
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
 
        if (o == null || getClass() != o.getClass()) 
            return false;
 
        CalcVariable that = (CalcVariable) o;
        return Objects.equals(calculation, that.calculation) && 
               Objects.equals(variable, that.variable);
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(calculation, variable);
    }
    
    //</editor-fold>

    /**
     * Проверяет корректность уже существующего значения переменной
     * @return корректность значения
     */
    public boolean isValueCorrect(){
        if (variable.getType() == VariableTypes.BT_NEXT || variable.getType() == VariableTypes.BT_PRINT) return true;
        if (variable.getType() == VariableTypes.CONDITION && value == 0 ) return false;
        if (value > variable.getValueMax() || value < variable.getValueMin()) return false;        
        return true;
    }
    
    /**
     * Проверяет корректность нового значения переменной
     * @param newValue новое значение
     * @return корректность значения
     */
    public boolean isValueCorrect(Double newValue){
        if (variable.getType() == VariableTypes.BT_NEXT || variable.getType() == VariableTypes.BT_PRINT) return true;
        if (variable.getType() == VariableTypes.CONDITION && newValue == 0 ) return false;
        if (newValue > variable.getValueMax() || newValue < variable.getValueMin()) return false;        
        return true;
    }
    
    @Override
    public int compareTo(CalcVariable o) {
        return this.variable.compareTo(o.getVariable());
    }
}
