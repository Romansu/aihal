package ru.rank.rodina.aihal.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import ru.rank.rodina.aihal.catalogs.Interfacing;
import ru.rank.rodina.aihal.catalogs.Transection;
import ru.rank.rodina.aihal.entities.manytomany.ObtRock;

/**
 * Объект для работы с выработкой
 */
@Entity
@Table (name="OBT")
public class Obt extends AbstractDB{  
    
    //<editor-fold defaultstate="collapsed" desc="Columns">
    @Id
    @SequenceGenerator(name="seqObt",sequenceName="OBT_SEQ", allocationSize = 1)        
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seqObt")  
    protected int id;
    
    @Column
    private String name;
   
    @Column
    @Enumerated(EnumType.STRING)
    private Interfacing interfacing;
    
    @Column(name="SECTION")
    @Enumerated(EnumType.STRING)
    private Transection transection;
    
    @Column
    private double B;
    
    @Column
    private double B1;
    
    @Column
    private double B2;
    
    @Column
    private double height;
    
    @Column
    private Boolean gkv;
    
    //</editor-fold>    
    
    //<editor-fold defaultstate="collapsed" desc="@OneTo">
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "obt")    
    private JRating jRating;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "obt", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Calc> calcs;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "obt", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ObtRock> rocks;
    
    //</editor-fold>    
       
    //<editor-fold defaultstate="collapsed" desc="Конструкторы">
    public Obt() {
        this.interfacing = Interfacing.NO;
        this.transection = Transection.RECTANGLE;
        this.gkv = true;
        this.calcs = new ArrayList();
        this.rocks = new ArrayList();
    }
    
    public Obt(String name) {
        this();
        this.name = name;
    }
    public Obt(int id, String name) {
        this(name);
        this.id = id;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    } 
    
    public Transection getTransection() {
        return this.transection;
    }
    
    public void setTransection(Transection transection) {
        this.transection = transection;
    }
    
    public Interfacing getInterfacing() {
        return this.interfacing;
    }
    
    public void setInterfacing(Interfacing interfacing) {
        this.interfacing = interfacing;
    }
    
    public double getB() {
        return this.B;
    }
    
    public void setB(double B) {
        this.B = B;
    }
    
    public double getB1() {
        return this.B1;
    }
    
    public void setB1(double B1) {
        this.B1 = B1;
    }
    
    public double getB2() {
        return this.B2;
    }
    
    public void setB2(double B2) {
        this.B2 = B2;
    }
    
    public double getHeight() {
        return this.height;
    }
    
    public void setHeight(double height) {
        this.height = height;
    }
    
    public Boolean getGKV() {
        return this.gkv;
    }
    
    public void setGKV(Boolean gkv) {
        this.gkv = gkv;
    }
    
    public JRating getJRating() {
        return jRating;
    }
    public void setJRating(JRating jRating) {
        this.jRating = jRating;
    }
    
    public List<Calc> getCalcs() {
        return this.calcs;
    }
    public void setCalcs(List<Calc> calcs) {
        this.calcs = calcs;
    }
    
    public void addCalc(Calc calc){
        this.calcs.add(calc);
    }
    
    public List<ObtRock> getObtRocks() {
        return this.rocks;
    }
    public void setObtRocks(List<ObtRock> value) {
        this.rocks = value;
    }
    
    public void addObtRock(ObtRock element){
        this.rocks.add(element);
    }
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="Override">
    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }
    
    @Override
        public boolean equals(Object obj){
            if (obj == null) return false;
            if (obj == this) return true;
            if (!(obj instanceof Obt))return false;
            Obt other = (Obt)obj;
            if (this.id == other.id 
                && this.name == other.name
                && this.jRating == other.jRating ) return true;
            return false;    
        }   
        
    @Override
    public int hashCode() {
        return Objects.hash(name, id, jRating);
    }   
    //</editor-fold>
  
}
