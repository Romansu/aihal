package ru.rank.rodina.aihal.vm;

import java.util.List;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import ru.rank.rodina.aihal.DAO.Updater.Hibernate.ObtRockUpdater;
import ru.rank.rodina.aihal.entities.Rock;

/**
 * Реализация для слоёв выработки
 */
public class ObtRockVM extends AbstractVM<ObtRockUpdater> implements Comparable<ObtRockVM>{    
    private ObjectProperty<Rock> rock;
    private IntegerProperty level;
    private IntegerProperty height;
    private IntegerProperty r;
    
    private ObtVM obtVM;
    
    //<editor-fold defaultstate="collapsed" desc="Конструкторы">
    public ObtRockVM() {
        this.rock = new SimpleObjectProperty();
        this.level = new SimpleIntegerProperty();
        this.height = new SimpleIntegerProperty();
        this.r = new SimpleIntegerProperty();
        setListeners();
    }    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
    public Rock getRock() {
        return rock.get();
    }

    public void setRock(Rock value) {
        this.rock.set(value);  
        
    }

    public ObjectProperty rockProperty() {
        return rock;
    }
    
    public int getLevel() {
        return level.get();
    }

    public void setLevel(int value) {
        this.level.set(value);
    }

    public IntegerProperty levelProperty() {
        return level;
    }
    
    public int getHeight() {
        return height.get();
    }

    public void setHeight(int value) {
        this.height.set(value);
    }

    public IntegerProperty heightProperty() {
        return height;
    }
    
    public int getR() {
        return r.get();
    }

    public void setR(int value) {
        this.r.set(value);
    }

    public IntegerProperty rProperty() {
        return r;
    }
    
    public void setObtVM(ObtVM value) {
        this.obtVM = value;
    }
    
    public ObtRockUpdater getUpdater() {
        return updater;
    }
    //</editor-fold >
             
    //<editor-fold defaultstate="collapsed" desc="Static">
    public static List<Rock> getAllRocks() {
        return ObtRockUpdater.getAllRocks();
    }   
            
    
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Обработка изменений"> 
    private void setListeners(){
        rock.addListener((observable) -> {checkChanges();});
        level.addListener((observable) -> {checkChanges();});
        height.addListener((observable) -> {checkChanges();});
    
    }
    
    /**
     * Проверяет изменения в данных выработки и передает управление на уровень работы с БД для сохранения изменений
     */
    private void checkChanges() {
        
        if(updater!=null)  { updater.setData(this); r.set(rock.get().getResist());}
    }
    //</editor-fold> 
    
    @Override
    public int compareTo(ObtRockVM o) {
        return level.get() > o.getLevel() ? 1 
              :level.get() == o.getLevel() ? 0 
              : -1;        
    }
    
    
}
