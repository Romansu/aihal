package ru.rank.rodina.aihal.vm;

import static java.lang.System.out;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import org.springframework.context.ApplicationContext;
import ru.rank.rodina.aihal.annotation.Setter;

/**
 * Реализация для рейтинга RMR
 */
public class JRatingVM {
    private IntegerProperty id;
    private IntegerProperty A1;
    private IntegerProperty A2;
    private IntegerProperty A3;
    private IntegerProperty A41;
    private IntegerProperty A42;
    private IntegerProperty A43;
    private IntegerProperty A44;
    private IntegerProperty A45;
    private IntegerProperty A5;
    private IntegerProperty B;
    private Boolean newItem;
    private Boolean changedItem;
    private ApplicationContext appContext;

    public JRatingVM( ) {
        this.id = new SimpleIntegerProperty();
        this.A1 = new SimpleIntegerProperty();
        this.A2 = new SimpleIntegerProperty();
        this.A3 = new SimpleIntegerProperty();
        this.A41 = new SimpleIntegerProperty();
        this.A42 = new SimpleIntegerProperty();
        this.A43 = new SimpleIntegerProperty();
        this.A44 = new SimpleIntegerProperty();
        this.A45 = new SimpleIntegerProperty();
        this.A5 = new SimpleIntegerProperty();
        this.B = new SimpleIntegerProperty();
        newItem = true;   
        changedItem = false;        
    }
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
    public int getId() {
        return id.get();
    }

    @Setter
    public void setId(int value) {
        this.id.set(value);
    }

    public IntegerProperty idProperty() {
        return id;
    }
    
    public int getA1() {
        return A1.get();
    }

    @Setter
    public void setA1(int value) {
        this.A1.set(value);
    }

    public IntegerProperty A1Property() {
        return A1;
    }
    
    public int getA2() {
        return A2.get();
    }

    @Setter
    public void setA2(int value) {
        this.A2.set(value);
    }

    public IntegerProperty A2Property() {
        return A2;
    }
    
    public int getA3() {
        return A3.get();
    }

    @Setter
    public void setA3(int value) {
        this.A3.set(value);
    }

    public IntegerProperty A3Property() {
        return A3;
    }
    
    public int getA41() {
        return A41.get();
    }

    @Setter
    public void setA41(int value) {
        this.A41.set(value);
    }

    public IntegerProperty A41Property() {
        return A41;
    }
    
    public int getA42() {
        return A42.get();
    }

    @Setter
    public void setA42(int value) {
        this.A42.set(value);
    }

    public IntegerProperty A42Property() {
        return A42;
    }
    
    public int getA43() {
        return A43.get();
    }

    @Setter
    public void setA43(int value) {
        this.A43.set(value);
    }

    public IntegerProperty A43Property() {
        return A43;
    }
    public int getA44() {
        return A44.get();
    }
    
    @Setter
    public void setA44(int value) {
        this.A44.set(value);
    }

    public IntegerProperty A44Property() {
        return A44;
    }
    
    public int getA45() {
        return A45.get();
    }
    
    @Setter
    public void setA45(int value) {
        this.A45.set(value);
    }

    public IntegerProperty A45Property() {
        return A45;
    }
    
    public int getA5() {
        return A5.get();
    }
    
    @Setter
    public void setA5(int value) {
        this.A5.set(value);
    }

    public IntegerProperty A5Property() {
        return A5;
    }
    
    public int getB() {
        return B.get();
    }
    
    @Setter
    public void setB(int value) {
        this.B.set(value);
    }

    public IntegerProperty BProperty() {
        return B;
    }
    
    public void setNewItem(boolean newItem) {
        this.newItem = newItem;        
    }
            
    public void setChangedItem(boolean newItem) {
        this.changedItem = newItem;        
    }
    
    public String getSum() {
        Integer sum = A1.get() + A2.get() + A3.get() + A5.get() + B.get() +
               A41.get() + A42.get() + A43.get() + A44.get() + A45.get();
        return sum.toString();
    }
    //</editor-fold>
    
    public boolean isNew(){
        return this.newItem;
    }
    
    public boolean isChanged(){
        return this.changedItem;
    }
   
    public boolean isFull() {
       Integer sum = A1.get() + A2.get() + A3.get() + A5.get() + B.get() +
               A41.get() + A42.get() + A43.get() + A44.get() + A45.get();
        if ( sum > 0) return true;
        return false;
    }
}
