package ru.rank.rodina.aihal.vm;

import ru.rank.rodina.aihal.DAO.FunctionsUpdater;

/**
 * Абстрактный класс для обработки данных из пользовательского интерфейса и передачи их на уровень работы с БД
 */
public abstract class AbstractVM <T extends FunctionsUpdater> {
    
    T updater;
    
    public void setUpdater(T updater) {
        this.updater = updater;
    }
    
}
