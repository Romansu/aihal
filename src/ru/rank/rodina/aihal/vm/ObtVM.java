
package ru.rank.rodina.aihal.vm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ru.rank.rodina.aihal.Aihal;
import ru.rank.rodina.aihal.DAO.Updater.Hibernate.ObtUpdater;
import ru.rank.rodina.aihal.catalogs.Interfacing;
import ru.rank.rodina.aihal.catalogs.Transection;
import ru.rank.rodina.aihal.ui.MainPaneController;
import ru.rank.rodina.aihal.util.Maths;

/**
 * Реализация для выработки
 */
public class ObtVM extends AbstractVM<ObtUpdater>{
    
    private IntegerProperty id;
    private StringProperty name;
    private DoubleProperty B;
    private DoubleProperty B1;
    private DoubleProperty B2;
    private DoubleProperty height;
    private BooleanProperty gkv;
    
    private StringProperty jRatingName;
    private StringProperty paramsName;
    
    private ObjectProperty<Interfacing> interfacing; 
    private ObjectProperty<Transection> transection;
    
    private JRatingVM jRatingVM;      
    private List<CalcVM> calcVMs = new ArrayList();
    private List<ObtRockVM> rockVMs = new LinkedList();    
    
    //<editor-fold defaultstate="collapsed" desc="Конструкторы">
    
    public ObtVM() {
        this.id = new SimpleIntegerProperty();
        this.name = new SimpleStringProperty();
        this.B = new SimpleDoubleProperty();
        this.B1 = new SimpleDoubleProperty();
        this.B2 = new SimpleDoubleProperty();
        this.height = new SimpleDoubleProperty();
        this.gkv = new SimpleBooleanProperty();
        this.jRatingName = new SimpleStringProperty();
        this.paramsName = new SimpleStringProperty();
        this.interfacing = new SimpleObjectProperty();
        this.transection = new SimpleObjectProperty();
        createParamsName();
        setListeners();
    }
    
    public ObtVM(int id, String name){
    //    this.calcVMs = new ArrayList();
        this.id = new SimpleIntegerProperty(id);
        this.name = new SimpleStringProperty(name);        
    }
    
    public ObtVM(int id, String name, ObtUpdater updater){
        this(id, name);
        this.updater = updater;         
    }
    
    public ObtVM(int id, String name, ObtUpdater updater, JRatingVM jRatingVM){
        this(id, name, updater);      
        this.jRatingVM = jRatingVM;
        this.jRatingName.set("Рейтинг массива RMR: "+jRatingVM.getSum());
        
    }
    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="get/set">
    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }
    
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }
    
    public Interfacing getInterfacing() {
        return this.interfacing.get();
    }

    public void setInterfacing(Interfacing interfacing) {
        this.interfacing.set(interfacing);
    }
    
    public ObjectProperty interfacingProperty() {
        return interfacing;
    }

    public Transection getTransection() {
        return this.transection.get();
    }

    public void setTransection(Transection transection) {
        this.transection.set(transection);
    }
    
    public ObjectProperty transectionProperty() {
        return transection;
    }
    
    public double getB() {
        return B.get();
    }

    public void setB(double value) {
        this.B.set(value);
        createParamsName();
    }

    public DoubleProperty bProperty() {
        return B;
    }
    
    public double getB1() {
        return B1.get();
    }

    public void setB1(double value) {
        this.B1.set(value);
    }

    public DoubleProperty b1Property() {
        return B1;
    }
    
    public double getB2() {
        return B2.get();
    }

    public void setB2(double value) {
        this.B2.set(value);
    }

    public DoubleProperty b2Property() {
        return B2;
    }
    
    public double getHeight() {
        return height.get();
    }

    public void setHeight(double value) {
        this.height.set(value);
    }

    public DoubleProperty heightProperty() {
        return height;
    }
    
    public Boolean getGKV() {
        return gkv.get();
    }

    public void setGKV(Boolean value) {
        this.gkv.set(value);
    }

    public BooleanProperty gkvProperty() {
        return gkv;
    }    
    
    public StringProperty jRatingNameProperty() {
        return jRatingName;
    }
    
     public StringProperty paramsNameProperty() {
        return paramsName;
    }
   
    public JRatingVM getJRatingVM() {
        return jRatingVM;
    }
    
    public void setJRatingVM(JRatingVM jRatingVM) {
        this.jRatingVM = jRatingVM;
        this.jRatingName.set("Рейтинг массива RMR: "+jRatingVM.getSum());
    }
   
    public List<CalcVM> getCalcVMs() {
        return calcVMs;
    } 
    
    public void setCalcVMs(List<CalcVM> calcVMs) {
        this.calcVMs = calcVMs;
    }
    
    public void addCalcVM(CalcVM calcVM) {
        this.calcVMs.add(calcVM);
    }
    
    public List<ObtRockVM> getObtRockVMs() {
        return rockVMs;
    } 
    
    public void setObtRockVMs(List<ObtRockVM> value) {
        this.rockVMs = value;
    }
    
    public void addObtRockVM(ObtRockVM value) {
        this.rockVMs.add(value);
    }
    
    public ObtUpdater getUpdater() {
        return this.updater;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Static">
    
    /**
     * Возвращает список выработок
     * @return выработки в наблюдаемом списке
     */
    public static ObservableList<ObtVM> getAll() {
        ObservableList<ObtVM> allObt = FXCollections.observableArrayList(); 
        allObt.addAll(ObtUpdater.createAll());
        return allObt; 
    }
    
    //</editor-fold >
    
    //<editor-fold defaultstate="collapsed" desc="add/delete"> 
    public static ObtVM addObt() {
        return ObtUpdater.createObt();        
    }
    
    public void deleteObt() {
        updater.deleteObt();        
    }
    
    public void addCalc() {
        calcVMs.add(updater.createCalc(this));
        MainPaneController mainPaneController = Aihal.getMainPaneController();
        mainPaneController.updateCenterPaneByVM(this);
    }
    
    public void deleteCalc(CalcVM calcVM) {
        updater.deleteCalc(calcVM);
        calcVMs.remove(calcVM);        
    }
    
    public ObtRockVM addRock(int newIndex) {
        return updater.createObtRock(newIndex, this);        
    }
    
    public void deleteRock(ObtRockVM obtRockVM) {
        updater.deleteObtRock(obtRockVM, this);        
    }
    
     //</editor-fold> 
        
    //<editor-fold defaultstate="collapsed" desc="Обработка изменений"> 
    
    private void setListeners() {
        name.addListener((observable, oldValue, newValue) -> {checkChanges();});
        B.addListener((observable, oldValue, newValue) -> {checkChanges();});
        B1.addListener((observable, oldValue, newValue) -> {checkChangesCountB();});
        B2.addListener((observable, oldValue, newValue) -> {checkChangesCountB();});
        gkv.addListener((observable, oldValue, newValue) -> {checkChanges();});        
        height.addListener((value) -> {checkChanges();});        
        interfacing.addListener((observable, oldValue, newValue) -> {checkChangesCountB();});
        transection.addListener((observable, oldValue, newValue) -> {checkChangesCountB();});        
    }
    
    /**
     * Проверяет изменения в данных выработки и передает управление на уровень работы с БД для сохранения изменений
     */
    private void checkChanges() {
        createParamsName();
        if(updater!=null)  updater.setData(this); 
    }
    
    /**
     * Проверяет изменения в данных рейтинга и передает управление на уровень работы с БД для сохранения изменений
     */
    public void checkChangesJRating() {
        this.jRatingName.set("Рейтинг массива RMR: "+jRatingVM.getSum());
        checkChanges();      
    }   
    
    /**
     * Проверяет изменения в типах и длинах сопряжений,
     * пересчитывает итоговую длину и передает управление на уровень работы с БД для сохранения изменений
     */
    private void checkChangesCountB(){
        countB();        
        checkChanges(); 
    }
    
    /**
     * Загружает в выработку расчёты
     */
    public void loadCalcVMs() {
        updater.loadCalcVMs(this);
    } 
    
    /**
     * Пересчитывает итоговую длину выработки из длин сопряжений
     */
    private void countB() {
        if (interfacing.get() == Interfacing.CONTIGUITY){
            B.set( Maths.round(Math.sqrt( Math.pow(B1.get(),2) + 0.5*Math.pow(B2.get(),2) ),1) );
            }         
        else if(interfacing.get() == Interfacing.INTERSECTION){
            B.set( Maths.round(Math.sqrt( Math.pow(B1.get(),2) + Math.pow(B2.get(),2) ), 1) );
            };         
    }
   
    //</editor-fold> 
     
    //<editor-fold defaultstate="collapsed" desc="Override"> 
     @Override
        public boolean equals(Object obj){
            if (obj == null) return false;
            if (obj == this) return true;
            if (!(obj instanceof ObtVM))return false;
            ObtVM other = (ObtVM)obj;
            if (this.id == other.id 
                && this.name == other.name
                && this.jRatingVM == other.jRatingVM ) return true;
            return false;    
        }   
        
    @Override
    public int hashCode() {
        return Objects.hash(name, id, jRatingVM);
    }
    //</editor-fold>

    /**
     * Создает строковое отображение параметров выработки 
     */
    private void createParamsName() {
        paramsName.set("Высота: " + height.getValue().toString() + "м., ширина: " + 
                B.getValue().toString());
    }
    
    public boolean isReadyForCalc() {
        return jRatingVM.isFull() && B.get()>0 && height.get() >0 && !rockVMs.isEmpty();
    }
    
}
