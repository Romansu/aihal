package ru.rank.rodina.aihal.vm;

import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import ru.rank.rodina.aihal.DAO.Updater.Hibernate.CalcUpdater;
import ru.rank.rodina.aihal.calculation.Calculation;
import ru.rank.rodina.aihal.entities.Method;
import ru.rank.rodina.aihal.entities.manytomany.CalcVariable;
import ru.rank.rodina.aihal.ui.CalcPaneController;

/**
 * Реализация для расчётов
 */
public class CalcVM extends AbstractVM<CalcUpdater>{
    private IntegerProperty id;
    private ObjectProperty<Method> mainMethod;
    private ObjectProperty<Method> additionalMethod; 
    private CalcPaneController ctrl;
    private Calculation calculation;
    
    //<editor-fold defaultstate="collapsed" desc="Конструкторы">
    public CalcVM() {
        this.id = new SimpleIntegerProperty();
        this.mainMethod = new SimpleObjectProperty();
        this.additionalMethod = new SimpleObjectProperty();
        setListeners();
    }
    
    public CalcVM(int id) {
        this();
        this.id.set(id);
    }
    //</editor-fold >
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
    public int getId() {
        return this.id.get();
    }
    public void setId (int id) {
        this.id.set(id);
    }
    
     public Method getMainMethod() {
        return this.mainMethod.get();
    }

    public void setMainMethod(Method value) {
        this.mainMethod.set(value);
    }
    
    public ObjectProperty mainMethodProperty() {
        return mainMethod;
    }

    public Method getAdditionalMethod() {
        return this.additionalMethod.get();
    }

    public void setAdditionalMethod(Method value) {
        this.additionalMethod.set(value);
    }
    
    public ObjectProperty additionalMethodProperty() {
        return additionalMethod;
    }
    
    public CalcUpdater getUpdater() {
        return updater;
    }
      
    @Override
    public void setUpdater(CalcUpdater value) {
        this.updater = value;
    }
    
    public Calculation getCalculation() {
        return this.calculation;
    }
    
    public void setCalculation(Calculation value) {
        this.calculation = value;
    }
    
    public void setCtrl(CalcPaneController value) {
        this.ctrl = value;
    }
    
    public List<Method> getAllMethods(Boolean isMain){
        return updater.getAllMethod(isMain);
    }   
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Обработка изменений">     
    private void setListeners(){
        mainMethod.addListener((observable) -> {checkChanges(); });
        additionalMethod.addListener((observable) -> {checkChanges(); });
    }
    
    /**
     * Проверяет изменения и передает управление на уровень работы с БД для сохранения изменений.
     * Переносит данные из vm в entity
     */
    private void checkChanges(){
        if(updater!=null)  updater.setData(this); 
    }
    /**
     * Передаёт управление на уровень работы с БД для сохранения изменений.
     * Записывает данные, которые уже есть в entity.
     */
    public void saveData() {
        if(updater!=null)  updater.setData();
    }
     
    //</editor-fold> 
    
    /**
     * Удаляет и заново создает в расчёте список переменных. 
     * Создаваемый список берётся из списка метода.
     */
    public void deleteAndCreateValues() {
        ctrl.clearVB();
        if(updater!=null)  updater.deleteAndCreateValues(this); 
        rewritePane(0);
    }
    
    /**
     * Удаляет  в расчёте список переменных. 
     */
    public void deleteValues() {
        ctrl.clearVB();
        if(updater!=null)  updater.deleteValues(this); 
    }
   
    /**
    * Очищает значения и визуальное отображение переменных в следующем прорисовываемом блоке.
    * Пересчитывает и прорисовывает следующий блок.
    * @param buttonIndex номер следующего блока
    */    
    public void rewriteNextPane(int buttonIndex) {
        int newIndex = buttonIndex;
        calculation.clearNextBlocks(newIndex);
        ctrl.crearVBChildren(newIndex);        
        rewritePane(newIndex);
    }
    /**
     * Пересчитывает и прорисовывает следующий блок.
     * @param index номер рассчитываемого блока
     */
    private void rewritePane(int index) {
        calculation.setActualIndex(index);  
        ctrl.addToVB(calculation.createCountingBlock(this));
    }
   
    /**
     * Создает визуальное отображение переменных по значением, уже содержащимся в базе.
     */
    public void createVariablesPane() {
        if (!updater.isVariablesEmpty()) calculation.rewriteCountingBlock(this, node -> ctrl.addToVB(node));
    }
}
