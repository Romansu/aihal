package ru.rank.rodina.aihal.calculation;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.VBox;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.rank.rodina.aihal.calculation.configs.ConfigFactory;
import ru.rank.rodina.aihal.catalogs.VariableTypes;
import ru.rank.rodina.aihal.entities.Calc;
import ru.rank.rodina.aihal.entities.Variable;
import ru.rank.rodina.aihal.entities.manytomany.CalcVariable;
import ru.rank.rodina.aihal.vm.CalcVM;

/**
 * Управление переменными и методамив расчёте.
 */
public class Calculation {
    
    private List<LinkedList> countingList;
    private Reckoner reckoner;
    private String actualContext;
    private int actualIndex = 0;
    private ApplicationContext context;
    
    //<editor-fold defaultstate="collapsed" desc="Конструкторы">
    public Calculation() {
        countingList = new LinkedList();
    }
    
    public Calculation(Calc calc) {
        countingList = new LinkedList();
        reckoner = new Reckoner(calc);
        actualContext = calc.getMethodMain().getScheme();
        context = new AnnotationConfigApplicationContext(ConfigFactory.getClass(actualContext));            
        createCountingList(calc);
        //context = new ClassPathXmlApplicationContext(actualContext);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
    public int getActualIndex() {
        return this.actualIndex;
    }
    
    public void setActualIndex(int value) {
        this.actualIndex = value;
    }
    
    
    
    //</editor-fold>
      
    /**
     * Разбирает полный список переменых данного расчёта на блоки,
     * которые будут выводиться на экран.
     * @param calc расчёт, переменные которого разбиваются на блоки
     */
    private void createCountingList(Calc calc) {
        List<CalcVariable> variables = calc.getCalcVariables();
        LinkedList<CalcVariable> oneBlock = new LinkedList();
        for (CalcVariable value:variables) {
            if(value.getVariable().getType() == VariableTypes.BT_NEXT || value.getVariable().getType() == VariableTypes.BT_PRINT){
                oneBlock.add(value);
                countingList.add(oneBlock);
                oneBlock = new LinkedList();    
            }
            else {
                oneBlock.add(value);
            }    
        }
    }
 
    /**
     * Создает визуальное отображение блока переменных на базе их типов с расчётом значений.
     * Если какое-то значение переменной некорректно, расчёт останавливается. 
     * Значение кнопок в конце блока равно индексу следующего блока, даже если расчёт завершился с некорректным значением.
     * @param calcVM объект-vm, связанный с панелью вывода
     * @return vBox блок переменных
     */
    public VBox createCountingBlock(CalcVM calcVM) {
        VBox vBox = new VBox();
        ObservableList<Node> children = vBox.getChildren();
        LinkedList<CalcVariable> variables = countingList.get(actualIndex);
        for(CalcVariable value: variables) {
            Variable variable = value.getVariable();
            value.setValue(count(value));
            children.add(variable.getType().create(value, calcVM));
            if (!value.isValueCorrect()) {
                children.add(VariableTypes.BT_STOP.create(value, calcVM));
                variables.getLast().setValue((double)actualIndex + 1);
                break;
            }
        }        
        return vBox;
    }
    
    /**
     * Создает визуальное отображение блока переменных на базе их типов с уже рассчитанными значениями.
     * Если какое-то значение переменной некорректно, расчёт останавливается. 
     * Значение кнопок в конце блока равно индексу следующего блока, даже если расчёт завершился с некорректным значением.
     * @param calcVM объект-vm, связанный с панелью вывода
     * @return vBox блок переменных
     */
    public void rewriteCountingBlock(CalcVM calcVM, Consumer<Node> addToVB) {
        VBox vBox;
        ObservableList<Node> children;
        for (LinkedList<CalcVariable> variables:countingList) {
            vBox = new VBox();
            children = vBox.getChildren();
            if (variables.getLast().getValue() == null || variables.getLast().getValue() == 0) break;
              
            for(CalcVariable value: variables) {
                Variable variable = value.getVariable();
                children.add(variable.getType().create(value, calcVM));
                if (!value.isValueCorrect()) {
                    children.add(VariableTypes.BT_STOP.create(value, calcVM));
                    variables.getLast().setValue((double)actualIndex + 1);
                    break;
                }
            } 
            addToVB.accept(vBox);
        }
    }  
    
    
    /**
     * Очищает значения переменных во всех блоках после указанного
     * @param actualIndex последний рассчитанный блок
     */
    public void clearNextBlocks(int actualIndex) {
        for(int i = actualIndex; i < countingList.size(); i++ ) {
            LinkedList<CalcVariable> variables = countingList.get(i);
            for(CalcVariable value: variables) {
                value.setValue(0.0);
            }  
        }
        
    }
    
    /**
     * Рассчитывает и значение переменной
     * @param value переменная, с ктоторой ведётся работа
     * @return Double рассчитанное значение
     */
    private Double count(CalcVariable value) {
        Double newValue;
        switch (value.getVariable().getType()) {
            case BT_NEXT:
                newValue = (double)actualIndex + 1;
                break;
            case BT_PRINT:
                newValue = (double)actualIndex;
                break;
            default:
                AbctractReckoner ar = (AbctractReckoner) context.getBean(value.getVariable().getMethod());
                newValue = ar.count(this.reckoner);
                break;
        }
        return newValue;
    }
  
    
}
