package ru.rank.rodina.aihal.calculation.methods;

import java.util.List;
import java.util.Map;
import ru.rank.rodina.aihal.calculation.AbctractReckoner;
import ru.rank.rodina.aihal.calculation.Reckoner;
import ru.rank.rodina.aihal.entities.manytomany.CalcVariable;
import ru.rank.rodina.aihal.entities.manytomany.ObtRock;

public class PvnAnker extends AbctractReckoner{

    @Override
    public Double count(Reckoner reckoner) {        
        Map<String, CalcVariable> variables = reckoner.getVariables();        
        
        return (variables.get("nanker").getValue() * variables.get("Na").getValue())/
               (variables.get("Ck").getValue() * reckoner.getCalc().getObt().getB());
    }
   
    
}
