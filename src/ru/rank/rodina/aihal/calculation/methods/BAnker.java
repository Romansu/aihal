package ru.rank.rodina.aihal.calculation.methods;

import ru.rank.rodina.aihal.calculation.AbctractReckoner;
import ru.rank.rodina.aihal.calculation.Reckoner;
import ru.rank.rodina.aihal.entities.manytomany.CalcVariable;

public class BAnker extends AbctractReckoner{

    @Override
    public Double count(Reckoner reckoner) {
        return reckoner.getCalc().getObt().getB();
    }
    
}
