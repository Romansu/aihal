package ru.rank.rodina.aihal.calculation.methods;

import ru.rank.rodina.aihal.calculation.AbctractReckoner;
import ru.rank.rodina.aihal.calculation.Reckoner;

public class Next extends AbctractReckoner{

    @Override
    public Double count(Reckoner reckoner) {
        return 1.0;
    }
    
}
