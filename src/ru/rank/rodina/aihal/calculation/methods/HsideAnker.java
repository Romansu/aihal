package ru.rank.rodina.aihal.calculation.methods;

import ru.rank.rodina.aihal.calculation.AbctractReckoner;
import ru.rank.rodina.aihal.calculation.Reckoner;
import ru.rank.rodina.aihal.catalogs.Transection;
import ru.rank.rodina.aihal.entities.Obt;

public class HsideAnker extends AbctractReckoner{

    @Override
    public Double count(Reckoner reckoner) {
        Obt obt = reckoner.getCalc().getObt();
        //Последний вариант примерный, поскольку точных расчёт пока не возможен
        if (obt.getTransection() == Transection.RECTANGLE) return obt.getHeight();
        else if(obt.getTransection() == Transection.ARCH) return obt.getHeight()*2/3;
        else return obt.getHeight()*7/8;
    }
    
}
