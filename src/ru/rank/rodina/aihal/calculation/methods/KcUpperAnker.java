package ru.rank.rodina.aihal.calculation.methods;

import java.util.Map;
import ru.rank.rodina.aihal.calculation.AbctractReckoner;
import ru.rank.rodina.aihal.calculation.Reckoner;
import ru.rank.rodina.aihal.entities.manytomany.CalcVariable;

public class KcUpperAnker extends AbctractReckoner{

    @Override
    public Double count(Reckoner reckoner) {
        Map<String, CalcVariable> variables = reckoner.getVariables();
        return variables.get("kc").getValue()*
               variables.get("kbvr").getValue()*
               variables.get("km").getValue();
    }
    
}
