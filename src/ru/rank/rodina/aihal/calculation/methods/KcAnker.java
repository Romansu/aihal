package ru.rank.rodina.aihal.calculation.methods;

import ru.rank.rodina.aihal.calculation.AbctractReckoner;
import ru.rank.rodina.aihal.calculation.Reckoner;
import ru.rank.rodina.aihal.entities.manytomany.CalcVariable;

public class KcAnker extends AbctractReckoner{

    @Override
    public Double count(Reckoner reckoner) {
        CalcVariable variable = reckoner.getVariables().get("kc");
        return variable.getVariable().getValueDefault();
    }
    
}
