package ru.rank.rodina.aihal.calculation.methods;

import java.util.Map;
import ru.rank.rodina.aihal.calculation.AbctractReckoner;
import ru.rank.rodina.aihal.calculation.Reckoner;
import ru.rank.rodina.aihal.entities.manytomany.CalcVariable;
import ru.rank.rodina.aihal.util.Maths;

public class NankersideAnker extends AbctractReckoner{

    @Override
    public Double count(Reckoner reckoner) {
        //TODO В инструкции более строгие границы, почему и что делать?
        Double b = reckoner.getCalc().getObt().getB();
        if (b<=3.5) return 3.0;
        else if(b>3.5 && b<=4.5) return 4.0;
        else if(b>4.5 && b<=5.5) return 5.0;
        else return countN(reckoner);
    }
    
    private Double countN(Reckoner reckoner){
        Map<String, CalcVariable> variables = reckoner.getVariables();  
        Double d = ((variables.get("hside").getValue() - 0.5) /
                variables.get("Pminside").getValue() );
        Long l = Math.round(d);
        
        return l.doubleValue();        
    }
    
}
