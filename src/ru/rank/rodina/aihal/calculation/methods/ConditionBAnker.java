package ru.rank.rodina.aihal.calculation.methods;

import ru.rank.rodina.aihal.calculation.AbctractReckoner;
import ru.rank.rodina.aihal.calculation.Reckoner;

public class ConditionBAnker extends AbctractReckoner{

    @Override
    public Double count(Reckoner reckoner) {
        Double h = reckoner.getCalc().getObt().getB();
        return h<=5?1.0:0;
    }
    
}
