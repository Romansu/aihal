package ru.rank.rodina.aihal.calculation.methods;

import ru.rank.rodina.aihal.calculation.AbctractReckoner;
import ru.rank.rodina.aihal.calculation.Reckoner;
import ru.rank.rodina.aihal.entities.manytomany.CalcVariable;

public class AngleAnker extends AbctractReckoner{

    @Override
    public Double count(Reckoner reckoner) {
        CalcVariable variable = reckoner.getVariables().get("angle");
        return variable.getVariable().getValueDefault();
    }
    
}
