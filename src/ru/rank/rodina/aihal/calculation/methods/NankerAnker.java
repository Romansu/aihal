package ru.rank.rodina.aihal.calculation.methods;

import ru.rank.rodina.aihal.calculation.AbctractReckoner;
import ru.rank.rodina.aihal.calculation.Reckoner;
import ru.rank.rodina.aihal.entities.manytomany.CalcVariable;

public class NankerAnker extends AbctractReckoner{

    @Override
    public Double count(Reckoner reckoner) {
        //TODO В инструкции более строгие границы, почему и что делать?
        Double b = reckoner.getCalc().getObt().getB();
        if (b<=4.5) return 4.0;
        else if(b>4.5 && b<=5.5) return 5.0;
        else return 6.0;
    }
    
}
