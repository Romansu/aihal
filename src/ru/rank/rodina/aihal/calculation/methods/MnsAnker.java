package ru.rank.rodina.aihal.calculation.methods;

import java.util.List;
import java.util.Map;
import ru.rank.rodina.aihal.calculation.AbctractReckoner;
import ru.rank.rodina.aihal.calculation.Reckoner;
import ru.rank.rodina.aihal.entities.Obt;
import ru.rank.rodina.aihal.entities.manytomany.CalcVariable;

public class MnsAnker extends AbctractReckoner{

    @Override
    public Double count(Reckoner reckoner) {        
        Map<String, CalcVariable> variables = reckoner.getVariables();  
        Obt obt = reckoner.getCalc().getObt();
        Double leftFactor = (1.6 * obt.getB() * variables.get("Y").getValue())/
                             variables.get("Rsrvzv").getValue();
        
        Double rightFactor = (variables.get("lambda").getValue() * obt.getHeight() * variables.get("kp").getValue()) /
                              variables.get("ky").getValue(); 
        
        return leftFactor * Math.sqrt(rightFactor) *  variables.get("kz").getValue();
    }
    
    
    
}
