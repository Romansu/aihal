package ru.rank.rodina.aihal.calculation.methods;

import java.util.List;
import java.util.Map;
import ru.rank.rodina.aihal.calculation.AbctractReckoner;
import ru.rank.rodina.aihal.calculation.Reckoner;
import ru.rank.rodina.aihal.entities.manytomany.CalcVariable;
import ru.rank.rodina.aihal.entities.manytomany.ObtRock;

public class RsrvzvAnker extends AbctractReckoner{

    @Override
    public Double count(Reckoner reckoner) {
        
        Map<String, CalcVariable> variables = reckoner.getVariables();        
        
        return sumRocks(reckoner.getCalc().getObt().getObtRocks())*
               variables.get("Kc").getValue()/
               reckoner.getCalc().getObt().getB();
    }
    
    private Double sumRocks(List<ObtRock> rocks ) {
        Double sum = 0.0;
        for(ObtRock value:rocks) {
            sum = sum + value.getRock().getResist()*value.getHeight();
        }
        return sum;
    }
    
}
