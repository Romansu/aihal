package ru.rank.rodina.aihal.calculation.methods;

import ru.rank.rodina.aihal.calculation.AbctractReckoner;
import ru.rank.rodina.aihal.calculation.Reckoner;

public class PminsideAnker extends AbctractReckoner{

    @Override
    public Double count(Reckoner reckoner) {
        Integer rmr = reckoner.getCalc().getObt().getJRating().getRating();
        switch(rmr){
            case 1:
                return 0.5;
            case 2:
                return 0.7;
            case 3:
                return 1.0;
            case 4:
                return 1.0;
            case 5:
                return 1.0;
            default:
                return 1.0;    
            
        }
    }
    
}
