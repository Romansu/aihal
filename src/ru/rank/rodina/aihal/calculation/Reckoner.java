package ru.rank.rodina.aihal.calculation;

import java.util.List;
import java.util.Map;
import ru.rank.rodina.aihal.entities.Calc;
import ru.rank.rodina.aihal.entities.manytomany.CalcVariable;

/**
 * Хранилище данных для всех методов подсчёта переменных.
 */
public class Reckoner {
    
    Calc calc;
    Map<String, CalcVariable> variables;
    
    public Reckoner(Calc calc) {
        this.calc = calc;
        this.variables = calc.getVariablesWithMethods();
    }
    
    //<editor-fold defaultstate="collapsed" desc="get/set">
    public Calc getCalc() {
        return this.calc;
    } 
    public Map<String, CalcVariable> getVariables(){
        return this.variables;
    }
    
    //</editor-fold>
    
}
