package ru.rank.rodina.aihal.calculation;

/**
 * Абстрактный класс для определения общей функции
 */
public abstract class AbctractReckoner {
    public abstract Double count(Reckoner reckoner);    
}
