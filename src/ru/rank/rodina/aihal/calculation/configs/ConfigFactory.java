package ru.rank.rodina.aihal.calculation.configs;

import java.util.HashMap;
import java.util.Map;

/**
 * Класс для сопроставления названия конфигурации конфигупационному файлу
 */
public class ConfigFactory {
    private static Map<String, Class> factory = new HashMap();
    static{
        factory.put("Anker", Anker.class);
    }
    
    public static Class getClass(String name) {
        return factory.get(name);
    }
    
}
