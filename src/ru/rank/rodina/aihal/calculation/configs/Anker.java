package ru.rank.rodina.aihal.calculation.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.rank.rodina.aihal.calculation.methods.*;

/**
 * Конфигурация для метода Анкерная крепь с решетчатой или сетчатой затяжкой
 */
@Configuration
public class Anker {
    
    @Bean(name = "conditionB")
    public ConditionBAnker conditionBAnker() {
        return new ConditionBAnker();
    } 
    
    @Bean(name = "kc")
    public KcAnker kcAnker() {
        return new KcAnker();
    } 
    
    @Bean(name = "kbvr")
    public KbvrAnker kbvrAnker() {
        return new KbvrAnker();
    } 
    
    @Bean(name = "km")
    public KmAnker kmAnker() {
        return new KmAnker();
    } 
    
    @Bean(name = "Kc")
    public KcUpperAnker kcUpperAnker() {
        return new KcUpperAnker();
    } 
    
    @Bean(name = "Rsrvzv")
    public RsrvzvAnker rsrvzvAnker() {
        return new RsrvzvAnker();
    } 
    
    @Bean(name = "B")
    public BAnker bAnker() {
        return new BAnker();
    } 
    
    @Bean(name = "Y")
    public YAnker yAnker() {
        return new YAnker();
    } 
    
    @Bean(name = "lambda")
    public LambdaAnker lambdaAnker() {
        return new LambdaAnker();
    } 
    
    @Bean(name = "ky")
    public KyAnker kyAnker() {
        return new KyAnker();
    } 
    
    @Bean(name = "kp")
    public KpAnker kpAnker() {
        return new KpAnker();
    } 
    
    @Bean(name = "kz")
    public KzAnker kzAnker() {
        return new KzAnker();
    } 
    
    @Bean(name = "next")
    public Next next() {
        return new Next();
    } 
    
    @Bean(name = "mns")
    public MnsAnker mnsAnker() {
        return new MnsAnker();
    } 
    
    @Bean(name = "lz")
    public LzAnker lzAnker() {
        return new LzAnker();
    } 
    
    @Bean(name = "lv")
    public LvAnker lvAnker() {
        return new LvAnker();
    } 
    
    @Bean(name = "la")
    public LaAnker laAnker() {
        return new LaAnker();
    } 
    
    @Bean(name = "nanker")
    public NankerAnker nankerAnker() {
        return new NankerAnker();
    } 
    
    @Bean(name = "Pmin")
    public PminAnker pminAnker() {
        return new PminAnker();
    } 
    
    @Bean(name = "Ck")
    public CkAnker ckAnker() {
        return new CkAnker();
    } 
    
    @Bean(name = "Na")
    public NaAnker naAnker() {
        return new NaAnker();
    } 
    
    @Bean(name = "Pvn")
    public PvnAnker pvnAnker() {
        return new PvnAnker();
    } 
    
    @Bean(name = "hside")
    public HsideAnker hsideAnker() {
        return new HsideAnker();
    } 
    
    @Bean(name = "angle")
    public AngleAnker angleAnker() {
        return new AngleAnker();
    } 
    
    @Bean(name = "kb")
    public KbAnker kbAnker() {
        return new KbAnker();
    } 
    
    @Bean(name = "hb")
    public HbAnker hbAnker() {
        return new HbAnker();
    } 
    
    @Bean(name = "lzb")
    public LzbAnker lzbAnker() {
        return new LzbAnker();
    } 
    
    @Bean(name = "lvb")
    public LvbAnker lvbAnker() {
        return new LvbAnker();
    } 
    
    @Bean(name = "lab")
    public LabAnker labAnker() {
        return new LabAnker();
    } 
    
    @Bean(name = "Pminside")
    public PminsideAnker pminsideAnker() {
        return new PminsideAnker();
    } 
    
    @Bean(name = "nankerside")
    public NankersideAnker nankersideAnker() {
        return new NankersideAnker();
    } 
    
    @Bean(name = "Cb")
    public CbAnker cbAnker() {
        return new CbAnker();
    } 
    
    @Bean(name = "print")
    public Print print() {
        return new Print();
    } 
    
}
