/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.rank.rodina.aihal;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ru.rank.rodina.aihal.ui.MainPaneController;

/**
 *
 */
public class Aihal extends Application {
    private static Stage primaryStage;
    private static MainPaneController mainPaneController;
    
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage stage) {
        primaryStage = stage;
        createAndLoadMainPane();        
    }
       
    /**
     * Создаёт основное окно и сохраняет контроллер основного окна в локальную переменную 
     */    
    public void createAndLoadMainPane() {        
      try {
            FXMLLoader loader = new FXMLLoader(Aihal.class.getResource("ui/MainPane.fxml"));
            AnchorPane mainPane = (AnchorPane) loader.load();
            mainPaneController = loader.getController();
            
            Scene scene = new Scene(mainPane);
            primaryStage.setScene(scene);
            primaryStage.show();
       } catch (IOException ex) {
            Logger.getLogger(Aihal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Возвращает контроллер главного окна 
     * @return контроллер главного окна
     */
    public static MainPaneController getMainPaneController() {
        return mainPaneController;
    }
    
    /**
     * Возвращает сцену основного окна
     * @return основная сцена
     */
    public static Stage getMainPaneStage() {
        return primaryStage;
    }
      
}
