package ru.rank.rodina.aihal.DAO;

import java.util.List;
import java.util.Map;
import ru.rank.rodina.aihal.entities.AbstractDB;

/**
 * Базовые функции для работы с БД * 
 * @param <T> Абстрактный класс для работы с данными из БД
 */
public interface FunctionsDAO <T extends AbstractDB>  {
    
    public void create(T obj);
    public void update(T obj);
    public void delete(T obj);
    public List getAll();
    public AbstractDB getById(int id);
    public void inicialize(T obj);
    
}
