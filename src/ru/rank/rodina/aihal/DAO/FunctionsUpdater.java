package ru.rank.rodina.aihal.DAO;

import ru.rank.rodina.aihal.entities.AbstractDB;
import ru.rank.rodina.aihal.vm.AbstractVM;

/**
 * Базовые функции для синхронизации данных между данными в программе и базой данных
 * @param <T> Абстрактный класс для работы с данными из БД
 * @param <K> Абстрактный класс для работы с данными с уровня пользователя
 */
public interface FunctionsUpdater <T extends AbstractDB, K extends AbstractVM> {
    
   // public boolean isEqual(K dataVM);
   // public List<T> getAll();
    public void setData(K dataVM);
    
}
