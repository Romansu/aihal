package ru.rank.rodina.aihal.DAO.Updater.Hibernate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.rank.rodina.aihal.DAO.FunctionsDAO;
import ru.rank.rodina.aihal.DAO.Hibernate.FactoryDaoHibernate;
import ru.rank.rodina.aihal.DAO.Updater.AbstractUpdater;
import ru.rank.rodina.aihal.entities.JRating;
import ru.rank.rodina.aihal.entities.Obt;
import ru.rank.rodina.aihal.vm.CalcVM;
import ru.rank.rodina.aihal.vm.JRatingVM;
import ru.rank.rodina.aihal.vm.ObtRockVM;
import ru.rank.rodina.aihal.vm.ObtVM;

/**
 * Реализация для класса Obt
 */
public class ObtUpdater extends AbstractUpdater<Obt,ObtVM>{
    
    FunctionsDAO daoObject; 

    //<editor-fold defaultstate="collapsed" desc="Конструкторы">
    public ObtUpdater() throws Exception {
        FactoryDaoHibernate factory = new FactoryDaoHibernate();
        daoObject = factory.getDAO(Obt.class); 
    }
    
    public ObtUpdater(Obt dataDB) throws Exception {
        this();
        this.dataDB = dataDB;
    }
    //</editor-fold>
    
    public Obt getObt() {
        return dataDB;
    }
 
    @Override
    public void setData(ObtVM dataVM) {       
        if(dataVM.getJRatingVM().isChanged()) {            
            if(dataDB.getJRating() == null) {
                dataDB.setJRating(jRatingVMToJRating(dataVM.getJRatingVM()));
                dataDB.getJRating().setObt(dataDB); 
            }
            else {
                updateJRatingByJRatingVM(dataDB.getJRating(), dataVM.getJRatingVM());
            }            
        }
        updateObtByObtVM(dataDB, dataVM);
        daoObject.update(dataDB); 
    }
    
    /**
     * Загружает в выработку расчёты-vm
     * @param obtVM выработка-vm
     */    
    public void loadCalcVMs(ObtVM obtVM) {        
        daoObject.inicialize(dataDB);
        obtVM.setCalcVMs(CalcUpdater.createAll(dataDB));        
    }
    
    //<editor-fold defaultstate="collapsed" desc="add/delete">
    /**
     * Удаляет выработку
     */
    public void deleteObt() {
        daoObject.delete(dataDB);
    }
    
    /**
     * Создаёт для выработки-vm расчёт-vm
     * @param obtVM выработка-vm
     * @return расчёт-vm
     */
    public CalcVM createCalc(ObtVM obtVM) {
       return CalcUpdater.createCalc(dataDB);       
    }   
   
    /**
     * Удаляет указанный расчёт
     * @param calcVM расчёт для удаления 
     */
    public void deleteCalc(CalcVM calcVM) {
       calcVM.getUpdater().deleteCalc(dataDB);
    }
    
    /**
     * Создаёт новый слой для указанной выработки
     * @param newIndex порядковый номер слоя
     * @param obtVM выработка-vm
     * @return слой-vm 
     */
    public ObtRockVM createObtRock(int newIndex, ObtVM obtVM) {
       ObtRockVM obtRockVM = ObtRockUpdater.createObtRock(newIndex, obtVM);
       obtVM.addObtRockVM(obtRockVM);
       refreshRockLevels(obtVM.getObtRockVMs());  
       return obtRockVM;
    }   
   
    /**
     * Удаляет указанный слой в указанной выработке
     * @param obtRockVM слой-vm
     * @param obtVM выаботка-vm
     */
    public void deleteObtRock(ObtRockVM obtRockVM, ObtVM obtVM) {        
        obtRockVM.getUpdater().deleteObtRock(dataDB);
        obtVM.getObtRockVMs().remove(obtRockVM); 
        refreshRockLevels(obtVM.getObtRockVMs());
    }
   
    //</editor-fold>
    
    /**
     * Переносит данные из VM в Entity
     * @param obt выработка-entity
     * @param obtVM выработка-vm 
     */
    private void updateObtByObtVM(Obt obt, ObtVM obtVM) {
        obt.setName(obtVM.getName());
        obt.setB(obtVM.getB());
        obt.setB1(obtVM.getB1());
        obt.setB2(obtVM.getB2());
        obt.setGKV(obtVM.getGKV());
        obt.setHeight(obtVM.getHeight());
        obt.setInterfacing(obtVM.getInterfacing());
        obt.setTransection(obtVM.getTransection());        
    }
       
     /**
     * Проверяет соответствие уровню в описании позиции в списке.
     * Поскольку длина списка не будет превышать пары десятков, существенных потерь времени от этой операции не будет.
     */
    private void refreshRockLevels(List<ObtRockVM> rockVMs){
        Collections.sort(rockVMs);
        for (int index=0; index < rockVMs.size(); index++) {
            ObtRockVM obtRockVM = rockVMs.get(index);
            if(obtRockVM.getLevel() != index + 1) {
                obtRockVM.setLevel(index+1);
                obtRockVM.getUpdater().setData(obtRockVM);
            }
        }        
    }
    
    //<editor-fold defaultstate="collapsed" desc="Static">
   
    /**
     * Возвращает список VM для всех имеющихся в базе выработок
     * @return Список VM для выработок
     */
    public static List<ObtVM> createAll() {
        List<ObtVM> allObt = new ArrayList(); 
        try {            
            FactoryDaoHibernate factory = new FactoryDaoHibernate();
            FunctionsDAO daoObject;
            daoObject = factory.getDAO(Obt.class);            
            allObt.addAll(obtToObtVMList(daoObject.getAll()));
        } catch (Exception ex) {
            Logger.getLogger(ObtVM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return allObt;
    }
    
    /**
     * Преобразует список объектов-entity в список объектов-vm
     * @param obts список объектов-entity 
     * @return список объектов-vm
     */
    private static List<ObtVM> obtToObtVMList(List<Obt> obts) {
        List<ObtVM> obtVMs = new ArrayList();       
        for (Obt element : obts){ 
            obtVMs.add(obtToObtVM(element));          
            };            
        return obtVMs;
    }
    
    /**
     * Преобразует объект-entity в объект-vm
     * @param obt объект-entity 
     * @return объект-vm
     */
    private static ObtVM obtToObtVM(Obt obt) {
        ObtVM tmp = new ObtVM();
        try {            
            tmp.setId(obt.getId());
            tmp.setName(obt.getName());            
            tmp.setB(obt.getB());
            tmp.setB1(obt.getB1());
            tmp.setB2(obt.getB2());
            tmp.setHeight(obt.getHeight());
            tmp.setGKV(obt.getGKV());
            tmp.setInterfacing(obt.getInterfacing());
            tmp.setTransection(obt.getTransection());
            tmp.setObtRockVMs(ObtRockUpdater.createAll(obt, tmp));
            tmp.setJRatingVM(createJRatingVM(obt)); 
            tmp.setUpdater(new ObtUpdater(obt));
            
        } catch (Exception ex) {
            Logger.getLogger(ObtUpdater.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tmp;
    }
    
    /**
     * Создаёт рейтинги RMR для указанной выработки
     * @param obt выработка-entity
     * @return рейтинги-vm
     */
    private static JRatingVM createJRatingVM(Obt obt) {
       // ApplicationContext appContext = new ClassPathXmlApplicationContext("SpringXMLConfigAOP.xml");
      //  JRatingVM jRatingVM = (JRatingVM) appContext.getBean("JRatingVM");
        JRatingVM jRatingVM = new JRatingVM();
        JRating jRating = obt.getJRating();
        if (jRating != null) {
            jRatingVM.setA1(jRating.getByName("A1"));
            jRatingVM.setA2(jRating.getByName("A2"));
            jRatingVM.setA3(jRating.getByName("A3"));
            jRatingVM.setA41(jRating.getByName("A41"));
            jRatingVM.setA42(jRating.getByName("A42"));
            jRatingVM.setA43(jRating.getByName("A43"));
            jRatingVM.setA44(jRating.getByName("A44"));
            jRatingVM.setA45(jRating.getByName("A45"));
            jRatingVM.setA5(jRating.getByName("A5"));
            jRatingVM.setB(jRating.getByName("B"));
            jRatingVM.setNewItem(false);
            jRatingVM.setChangedItem(false);
        };
        return jRatingVM;
    }
    
    /**
     * Преобразует рейтинг-vm в рейтинг-entity
     * @param vm объект-vm 
     * @return объект-entity
     */
    private static JRating jRatingVMToJRating(JRatingVM vm){
        JRating jRating = new JRating();
        updateJRatingByJRatingVM(jRating, vm);
        return jRating;
    }
    
    /**
     * Переносит данные из VM в entity
     * @param calc объект-entity
     * @param calcVM объект-vm 
     */  
    private static void updateJRatingByJRatingVM(JRating jRating, JRatingVM jRatingVM) {
        jRating.setByName("A1", jRatingVM.getA1());
        jRating.setByName("A2", jRatingVM.getA2());
        jRating.setByName("A3", jRatingVM.getA3());
        jRating.setByName("A41", jRatingVM.getA41());
        jRating.setByName("A42", jRatingVM.getA42());
        jRating.setByName("A43", jRatingVM.getA43());
        jRating.setByName("A44", jRatingVM.getA44());
        jRating.setByName("A45", jRatingVM.getA45());
        jRating.setByName("A5", jRatingVM.getA5());
        jRating.setByName("B", jRatingVM.getB());  
   }
    
    /**
     * Создает новую выработку
     * @return выработка-vm
     */
    public static ObtVM createObt() {
        Obt newObt = new Obt("Новая выработка");
        try {            
            FactoryDaoHibernate factory = new FactoryDaoHibernate();
            FunctionsDAO daoObject;   
            daoObject = factory.getDAO(Obt.class);
            daoObject.create(newObt);            
        } catch (Exception ex) {
            Logger.getLogger(ObtUpdater.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obtToObtVM(newObt);
    }
        
    //</editor-fold>
    
}
