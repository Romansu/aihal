package ru.rank.rodina.aihal.DAO.Updater.Hibernate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.rank.rodina.aihal.DAO.FunctionsDAO;
import ru.rank.rodina.aihal.DAO.Hibernate.FactoryDaoHibernate;
import ru.rank.rodina.aihal.DAO.Updater.AbstractUpdater;
import ru.rank.rodina.aihal.entities.Obt;
import ru.rank.rodina.aihal.entities.Rock;
import ru.rank.rodina.aihal.entities.manytomany.ObtRock;
import ru.rank.rodina.aihal.vm.ObtRockVM;
import ru.rank.rodina.aihal.vm.ObtVM;

/**
 * Реализация для класса ObtRock
 */
public class ObtRockUpdater extends AbstractUpdater<ObtRock, ObtRockVM>{
    
    
    private static List<Rock> allRocks;
    static{
        allRocks = createAllRocks();
    }
    FunctionsDAO daoObject; 
    
    //<editor-fold defaultstate="collapsed" desc="Конструкторы">
    public ObtRockUpdater() throws Exception {
        FactoryDaoHibernate factory = new FactoryDaoHibernate();
        daoObject = factory.getDAO(ObtRock.class); 
    }
    
    public ObtRockUpdater(ObtRock value) throws Exception {
        this();
        this.dataDB = value;
    }
    //</editor-fold>
    
    @Override
    public void setData(ObtRockVM dataVM) {
        updateObtRockVMTByObtRock(dataDB, dataVM);
        daoObject.update(dataDB);
    }
    
    /**
     * Удаляет один слой у расчёта. 
     * @param obt выработка, которой принадлежит расчёт, с которого удаляется слой 
     */
    public void deleteObtRock(Obt obt){        
        obt.getObtRocks().remove(dataDB);
        daoObject.delete(dataDB);
    }
    
    /**
     * Переносит данные из VM в entity
     * @param obtRock объект-entity
     * @param obtRockVM объект-vm  
     */
    private void updateObtRockVMTByObtRock(ObtRock obtRock, ObtRockVM obtRockVM) {
        obtRock.setHeight(obtRockVM.getHeight());
        obtRock.setLevel(obtRockVM.getLevel());
        obtRock.setRock(obtRockVM.getRock());        
    }
     
    //<editor-fold defaultstate="collapsed" desc="Static">
    /**
     * Возвращает полный список имеющихся почв    
     * @return список почв
     */
    public static List<Rock> getAllRocks() {
        return allRocks;
    }
    
    /**
     * Создаёт полный список имеющихся почв 
     * @return список почв
     */
    private static List<Rock> createAllRocks() {
        List<Rock> rocks = new ArrayList();        
        try {
            FactoryDaoHibernate factory = new FactoryDaoHibernate();
            FunctionsDAO rockDAO = factory.getDAO(Rock.class);
            rocks = rockDAO.getAll();
        } catch (Exception ex) {
            Logger.getLogger(ObtRockUpdater.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rocks;       
    }    
    
    /**
     * Создает полный список почв для данной выработки
     * @param obt объект-entity
     * @param obtVM объект-vm
     * @return список почв-vm для определённой выработки
     */
    public static List<ObtRockVM> createAll(Obt obt, ObtVM obtVM) {        
        return obtRockToObtRockVMList(obt.getObtRocks(), obtVM);        
    }
    
    /**
     * Преобразует список объектов-entity в список объектов-vm
     * @param obtRocks список объектов-entity
     * @param obtVM список объектов-vm
     * @return 
     */
    private static List<ObtRockVM> obtRockToObtRockVMList(List<ObtRock> obtRocks, ObtVM obtVM) {
        List<ObtRockVM> obtRockVMs = new LinkedList();   
        for(ObtRock element : obtRocks) {
            obtRockVMs.add(obtRockToObtRockVM(element, obtVM));
        }
        Collections.sort(obtRockVMs);
        return obtRockVMs;
    }
    
    /**
     * Преобразует объект-entity в объект-vm
     * @param obtRock объект-entity 
     * @param obtVM список объектов-vm
     * @return 
     */
    private static ObtRockVM obtRockToObtRockVM(ObtRock obtRock, ObtVM obtVM) {
        ObtRockVM obtRockVM = new ObtRockVM();
        try {    
            obtRockVM.setHeight(obtRock.getHeight());
            obtRockVM.setRock(obtRock.getRock());
            obtRockVM.setLevel(obtRock.getLevel());
            obtRockVM.setR(obtRock.getRock().getResist());
            obtRockVM.setObtVM(obtVM);
            obtRockVM.setUpdater(new ObtRockUpdater(obtRock)); 
        } catch (Exception ex) {
            Logger.getLogger(ObtRockUpdater.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obtRockVM;        
    }
    
    /**
     * Создаёт новый слой в выработке
     * @param newIndex порядковый номер слоя
     * @param obtVM выработка-vm
     * @return слой-vm
     */
    public static ObtRockVM createObtRock(int newIndex, ObtVM obtVM) { 
        Obt obt = obtVM.getUpdater().getObt();
        ObtRock obtRock = new ObtRock(obt, allRocks.get(0));
        obtRock.setLevel(newIndex);
        
        try {            
            FactoryDaoHibernate factory = new FactoryDaoHibernate();
            FunctionsDAO daoObject;   
            daoObject = factory.getDAO(ObtRock.class);
            daoObject.create(obtRock);            
        } catch (Exception ex) {
            Logger.getLogger(ObtUpdater.class.getName()).log(Level.SEVERE, null, ex);
        }
        obt.addObtRock(obtRock);
        return obtRockToObtRockVM(obtRock, obtVM);
        
    }
    //</editor-fold>
}
