package ru.rank.rodina.aihal.DAO.Updater.Hibernate;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.rank.rodina.aihal.DAO.FunctionsDAO;
import ru.rank.rodina.aihal.DAO.Hibernate.FactoryDaoHibernate;
import ru.rank.rodina.aihal.DAO.Hibernate.MethodDAO;
import ru.rank.rodina.aihal.DAO.Updater.AbstractUpdater;
import ru.rank.rodina.aihal.calculation.Calculation;
import ru.rank.rodina.aihal.entities.Calc;
import ru.rank.rodina.aihal.entities.Method;
import ru.rank.rodina.aihal.entities.Obt;
import ru.rank.rodina.aihal.vm.CalcVM;

/**
 * Реализация для класса calc
 */
public class CalcUpdater extends AbstractUpdater<Calc,CalcVM>{
    
    FunctionsDAO daoObject;
    
    //<editor-fold defaultstate="collapsed" desc="Конструкторы">
    public CalcUpdater() throws Exception {
        FactoryDaoHibernate factory = new FactoryDaoHibernate();
        daoObject = factory.getDAO(Calc.class); 
    }
    
    public CalcUpdater(Calc dataDB) throws Exception {
        this();
        this.dataDB = dataDB;
    }
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Работа с БД">
    @Override
    public void setData(CalcVM dataVM) {
        updateCalcByCalcVM(dataDB, dataVM);
        daoObject.update(dataDB);        
    }  
    
    public void setData() {
        daoObject.update(dataDB);
    }
    
    public void deleteAndCreateValues(CalcVM calcVM) {
        deleteValues(calcVM);
        dataDB.setVariables(dataDB.getMethodMain().getVariables());
        daoObject.update(dataDB);
        calcVM.setCalculation(new Calculation(dataDB));
    }
    
    public void deleteValues(CalcVM calcVM) {
        dataDB.clearVariables();
        daoObject.update(dataDB);
    }
   
    /**
     * Удаляет один расчёт. Удаляется из списка расчётов выработки и из БД.
     * @param obt выработка, которой принадлежал удаляемый расчёт 
     */
    public void deleteCalc(Obt obt) {
        obt.getCalcs().remove(dataDB);
        daoObject.delete(dataDB);         
    }
    
    /**
     * Получает всех методы для данного расчёта
     * @param isMain будет ли метод основным или дополнительным
     * @return список всех подходящих методов
     */
    public List<Method> getAllMethod(Boolean isMain) {
        List<Method> methods = new ArrayList();
        try {
            
            FactoryDaoHibernate factory = new FactoryDaoHibernate();            
            //TODO Как обойтись без явного задания типа?
            MethodDAO methodDAO = (MethodDAO) factory.getDAO(Method.class);
            methods = methodDAO.getByCalc(dataDB, isMain);            
        } catch (Exception ex) {
            Logger.getLogger(CalcUpdater.class.getName()).log(Level.SEVERE, null, ex);
        }
        return methods;
    } 
    
    
    //</editor-fold>
    
    /**
     * Переносит данные из VM в entity
     * @param calc объект-entity
     * @param calcVM объект-vm 
     */
    private void updateCalcByCalcVM(Calc calc, CalcVM calcVM){
        calc.setMethodMain(calcVM.getMainMethod());
        calc.setMethodAdditional(calcVM.getAdditionalMethod());        
    } 
   
    public boolean isVariablesEmpty() {
        return dataDB.isVariablesEmpty();
    }
    
    //<editor-fold defaultstate="collapsed" desc="Static"> 
    
    /**
     * Создаёт полный список расчётов для данной выработки
     * @param obt выработка
     * @return список расчётов-vm
     */
    public static List<CalcVM> createAll(Obt obt) {
       return CalcToCalcVMList(obt.getCalcs());
    }
    
    /**
     * Преобразует список объектов-entity в список объектов-vm
     * @param calcs список объектов-entity 
     * @return список объектов-vm
     */
    private static List<CalcVM> CalcToCalcVMList(List <Calc> calcs){
        List<CalcVM> calcVMs = new ArrayList();
        //  if (calcs!=null)
        for (Calc element : calcs){
            calcVMs.add(CalcToCalcVM(element));
        }        
        return calcVMs;
    }
    
    /**
     * Преобразует объект-entity в объект-vm
     * @param calcs объект-entity 
     * @return объект-vm
     */
    private static CalcVM CalcToCalcVM(Calc calc) {
        CalcVM calcVM = new CalcVM();
        try {
            calcVM.setId(calc.getId());             
            calcVM.setMainMethod(calc.getMethodMain());
            calcVM.setAdditionalMethod(calc.getMethodAdditional());
            calcVM.setUpdater(new CalcUpdater(calc));
            if (calc.isVariablesEmpty())  calcVM.setCalculation(new Calculation());
            else calcVM.setCalculation(new Calculation(calc));
        } catch (Exception ex) {
            Logger.getLogger(CalcUpdater.class.getName()).log(Level.SEVERE, null, ex);
        }
        return calcVM;
    }

    /**
     * Создаёт новый расчёт
     * @param obt выработка, для которой создаётся расчёт
     * @return расчет-vm
     */
    public static CalcVM createCalc(Obt obt) {
        Calc newCalc = new Calc();
        newCalc.setObt(obt);
        obt.addCalc(newCalc);
        try {            
            FactoryDaoHibernate factory = new FactoryDaoHibernate();
            FunctionsDAO daoObject;   
            daoObject = factory.getDAO(Calc.class);
            daoObject.create(newCalc);            
        } catch (Exception ex) {
            Logger.getLogger(ObtUpdater.class.getName()).log(Level.SEVERE, null, ex);
        }
        return CalcToCalcVM(newCalc);        
    }
   
   //</editor-fold> 
    
}
