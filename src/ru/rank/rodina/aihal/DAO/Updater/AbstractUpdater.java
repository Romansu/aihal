package ru.rank.rodina.aihal.DAO.Updater;

import ru.rank.rodina.aihal.DAO.FunctionsUpdater;
import ru.rank.rodina.aihal.entities.AbstractDB;
import ru.rank.rodina.aihal.vm.AbstractVM;

/**
 * Абстрактный класс для реализации синхронизации данных программы и бд
 * @param <T> Абстрактный класс для работы с данными из БД
 * @param <K> Абстрактный класс для работы с данными с уровня пользователя
 */

public abstract class AbstractUpdater <T extends AbstractDB, K extends AbstractVM> implements FunctionsUpdater<T,K> {
    
    protected T dataDB; 

    public void setDataDB(T dataDB){
        this.dataDB = dataDB;
    };
   
    
    
}
