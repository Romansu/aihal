package ru.rank.rodina.aihal.DAO;

/**
 * Интерфейс фабрики DAO объектов
 */
public interface FactoryDAO {
    public interface CreatorDAO {
        public FunctionsDAO create();
    }
    
    public FunctionsDAO getDAO(Class adoClass) throws Exception;
    
}
