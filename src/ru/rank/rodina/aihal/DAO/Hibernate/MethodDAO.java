package ru.rank.rodina.aihal.DAO.Hibernate;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import ru.rank.rodina.aihal.entities.Calc;
import ru.rank.rodina.aihal.entities.Method;
import ru.rank.rodina.aihal.util.HibernateUtil;

/**
 * Реализация для класса Method
 */
public class MethodDAO extends HibernateDAO<Method>{

    @Override
    public List getAll() {
        Transaction transaction = null;
        ArrayList<Method> selectedItems = new ArrayList();
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            //selectedItems = (ArrayList<Obt>)session.createQuery("from Obt order by id").list();
            selectedItems = (ArrayList<Method>)session.createQuery("from Method order by id").list();
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
        return selectedItems;
    }

    @Override
    public Method getById(int id) {
        Transaction transaction = null;
        Method selectedItem;
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            selectedItem = (Method)session.get(Method.class, id);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
        return selectedItem;
    }
   
    @Override
    public void inicialize(Method obj) {
    }
    
    /**
     * Получение всех методов, соответстующих по определённым параметрам указанному расчёту
     * @param calc расчёт, для которого проводится выборка
     * @param isMain будет ли метод основным или дополнительным
     * @return список всех подходящих методов
     */
    public List getByCalc(Calc calc, Boolean isMain) {
        Transaction transaction = null;
        List<Method> selectedItems = new ArrayList();
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            selectedItems = session.createSQLQuery(getByCalcSQL(isMain)).addEntity(Method.class)
                    .setParameter("interfacing", (calc.getObt().getInterfacing()==null)?0:calc.getObt().getInterfacing().getDBValue())
                    .setParameter("gkv", (calc.getObt().getGKV())?1:0)
                    .setParameter("RMR", calc.getObt().getJRating().getSum())
                    .list();
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
        return selectedItems;
        
    }
    
    /**
     * Генерирует sql-строку с определёнными параметрами для получения из базы списка методов
     * @param isMain будет ли метод основным или дополнительным
     * @return список всех подходящих методов
     */
    private String getByCalcSQL(Boolean isMain) {
        StringBuilder sql = new StringBuilder( "SELECT ID, NAME, SCHEME FROM METHOD WHERE ");
        if(isMain) sql.append("MAIN = 1 ");
        else sql.append("ADDITIONAL = 1 ");
        sql.append("AND INTERFACING = :interfacing AND GKV = :gkv AND "
                + "MIN_RMR <= :RMR AND MAX_RMR >= :RMR" );
        return sql.toString();
    }
        
    
    
}
