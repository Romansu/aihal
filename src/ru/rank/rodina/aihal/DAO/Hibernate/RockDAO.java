package ru.rank.rodina.aihal.DAO.Hibernate;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.rank.rodina.aihal.entities.Rock;
import ru.rank.rodina.aihal.util.HibernateUtil;

/**
 * Реализация для класса Rock
 */
public class RockDAO extends HibernateDAO<Rock>{

    @Override
    public List getAll() {
        Transaction transaction = null;
        ArrayList<Rock> selectedItems = new ArrayList();
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            selectedItems = (ArrayList<Rock>)session.createQuery("from Rock order by id").list();
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
        return selectedItems;
    }

    @Override
    public Rock getById(int id) {
        Transaction transaction = null;
        Rock selectedItem;
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            selectedItem = (Rock)session.get(Rock.class, id);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
        return selectedItem;
    }

    @Override
    public void inicialize(Rock obj) {
    }
    
}
