package ru.rank.rodina.aihal.DAO.Hibernate;

import java.util.HashMap;
import ru.rank.rodina.aihal.DAO.FactoryDAO;
import ru.rank.rodina.aihal.DAO.FunctionsDAO;
import ru.rank.rodina.aihal.entities.*;
import ru.rank.rodina.aihal.entities.manytomany.ObtRock;

/**
 * Реализация абстрактной фабрики
 * в creators хранятся связки вида класс + метод-создатель 
 */
public class FactoryDaoHibernate implements FactoryDAO{
    
    private HashMap <Class, CreatorDAO> creators;
    
    public FactoryDaoHibernate() {
        creators = new HashMap<Class, CreatorDAO>();
        creators.put(Obt.class, () -> new ObtDAO());
        creators.put(Calc.class, () -> new CalcDAO());
        creators.put(Method.class, () -> new MethodDAO());
        creators.put(ObtRock.class, () -> new ObtRockDAO());
        creators.put(Rock.class, () -> new RockDAO());
        
    }
    
    /**
    * Создание нового объекта по выбранному классу
    * @param adoClass класс, для которого ищем метод-создатель
    * @return новый объект, реализующий общий интерфейс для работы с источником данных
    */
    @Override
    public FunctionsDAO getDAO(Class adoClass) throws Exception {
        CreatorDAO  creator = creators.get(adoClass);
        if (creator == null) {
            throw new Exception("Фабрика не определена для класса " + adoClass);
        }
        return creator.create();
    }
    
}
