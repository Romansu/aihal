package ru.rank.rodina.aihal.DAO.Hibernate;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.rank.rodina.aihal.entities.Obt;
import ru.rank.rodina.aihal.entities.Rock;
import ru.rank.rodina.aihal.entities.manytomany.ObtRock;
import ru.rank.rodina.aihal.util.HibernateUtil;

/**
 * Реализация для класса ObtRock
 */
public class ObtRockDAO extends HibernateDAO<ObtRock>{

    @Override
    public List getAll() {
        Transaction transaction = null;
        ArrayList<ObtRock> selectedItems = new ArrayList();
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            selectedItems = (ArrayList<ObtRock>)session.createQuery("from ObtRock order by obt, level").list();
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
        return selectedItems;
    }

    @Override
    public ObtRock getById(int id) {
        return null;        
    }

    @Override
    public void inicialize(ObtRock obj) {        
    }
    
    @Override
    public void create(ObtRock obj) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            Obt obt = obj.getObt();
            Rock rock  = obj.getRock();
            session.merge(obt);
            session.merge(rock);
            session.persist(obj);            
            transaction.commit();          
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
    }
    
}
