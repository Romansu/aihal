package ru.rank.rodina.aihal.DAO.Hibernate;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.rank.rodina.aihal.entities.Calc;
import ru.rank.rodina.aihal.util.HibernateUtil;

/**
 * Реализация для класса Calc
 */
public class CalcDAO extends HibernateDAO<Calc>{

    @Override
    public List getAll() {
        Transaction transaction = null;
         ArrayList<Calc> selectedItems = new ArrayList();
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            //selectedItems = (ArrayList<Obt>)session.createQuery("from Obt order by id").list();
            selectedItems = (ArrayList<Calc>)session.createQuery("from Calc order by id").list();
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
        return selectedItems;
    }

    @Override
    public Calc getById(int id) {
        Transaction transaction = null;
        Calc selectedItem;
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            selectedItem = (Calc)session.get(Calc.class, id);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
        return selectedItem;
    }

    @Override
    public void inicialize(Calc obj) {
        
    }
    
}
