package ru.rank.rodina.aihal.DAO.Hibernate;

import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.rank.rodina.aihal.entities.Obt;
import ru.rank.rodina.aihal.util.HibernateUtil;

/**
 * Реализация для класса Obt
 */
public class ObtDAO extends HibernateDAO<Obt>{
    
    public ObtDAO() {
        
    }

    @Override
    public List getAll() {
         Transaction transaction = null;
         ArrayList<Obt> selectedItems = new ArrayList();
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            selectedItems = (ArrayList<Obt>)session.createQuery("from Obt order by id").list();
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
        return selectedItems;
    }

    @Override
    public Obt getById(int id) {
         Transaction transaction = null;
         Obt selectedItem;
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            selectedItem = (Obt)session.get(Obt.class, id);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
        return selectedItem;
    }
    
    @Override
    public void inicialize(Obt obt){
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            session.update(obt);
            Hibernate.initialize(obt.getCalcs());
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
        
    }
    
}
