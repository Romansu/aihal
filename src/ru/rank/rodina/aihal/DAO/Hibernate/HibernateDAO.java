package ru.rank.rodina.aihal.DAO.Hibernate;

import java.util.List;
import java.util.Map;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.rank.rodina.aihal.DAO.FunctionsDAO;
import ru.rank.rodina.aihal.entities.AbstractDB;
import ru.rank.rodina.aihal.util.HibernateUtil;

/**
 * Абстрактная базовая реализация взаимодействия с БД с использованием Hibernate
 * @param <T> Абстрактный класс для работы с данными из БД
 */
public abstract class HibernateDAO<T extends AbstractDB> implements FunctionsDAO<T>{
    
    /**
     * Создаёт новую запись в БД и устанавливает сгенерированный id. Используется persist.
     * @param obj Объект-entity для записи в базу
     */
    @Override
    public void create(T obj) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            session.persist(obj);            
            transaction.commit();          
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
    }
    
    /**
     * Обновляет существующую  запись в БД. Используется update.
     * @param obj Объект-entity для обновления в базе
     */
    @Override
    public void update(T obj) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            session.update(obj);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
    }
    
    /**
     * Удаляет существующую запись в БД. Используется delete.
     * @param obj Объект-entity для удаления
     */
    @Override
    public void delete(T obj) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction  = session.beginTransaction();
            session.delete(obj);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            throw e;
        }
    }

    /**
     * Получает полный список объектов из БД
     * @return Список полученных объектов-entity
     */
    @Override
    public abstract List getAll();

    /**
     * Получает объект по его Id
     * @param id первичный ключ
     * @return объект-entity
     */
    @Override
    public abstract T getById(int id);

    /**
     * Дозагружает информацию ленивой загрузки
     * @param obj объект, поля которого догружаются
     */
    @Override
    public abstract void inicialize(T obj);    
    
}
